/*
GLFrame.h

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright (c) 2013, Todd V. Rovito
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef GLFrame_h
#define GLFrame_h

#include <math3d.h>
#include <stdbool.h>

struct GLFrameStruct {
    M3DVector3f vOrigin;        // where am i?
    M3DVector3f vForward;       // where am I going?
    M3DVector3f vUp;            // which way is up?
};

typedef struct GLFrameStruct GLFrame;

void glFrameInit(GLFrame *frame);

// set and get location ***************************
void glFrameSetOrigin(GLFrame *frame, const M3DVector3f vPoint);
void glFrameSetOriginFromPoints(GLFrame *frame, float x, float y, float z);
void glFrameGetOrigin(GLFrame *frame, M3DVector3f vPoint);
float glFrameGetOriginX(GLFrame *frame);
float glFrameGetOriginY(GLFrame *frame);
float glFrameGetOriginZ(GLFrame *frame);

// set and get forward direction *******************
void glFrameSetForwardVector(GLFrame *frame, const M3DVector3f vDirection);
void glFrameSetForwardVectorFromPoints(GLFrame *frame, float x, float y, float z);
void glFrameGetForwardVector(GLFrame *frame, M3DVector3f vVector);

// set and get up direction
void glFrameSetUpVector(GLFrame *frame, const M3DVector3f vDirection);
void glFrameSetUpVectorFromPoints(GLFrame *frame, float x, float y, float z);
void glFrameGetUpVector(GLFrame *frame, M3DVector3f vVector);

// get axes
void glFrameGetZAxis(GLFrame *frame, M3DVector3f vVector);
void glFrameGetYAxis(GLFrame *frame, M3DVector3f vVector);
void glFrameGetXAxis(GLFrame *frame, M3DVector3f vVector);

// move forward (along Z axis)
void glFrameMoveForward(GLFrame *frame, float fDelta);

// move along the y axis
void glFrameMoveUp(GLFrame *frame, float fDelta);

// move along the x axis
void glFrameMoveRight(GLFrame *frame, float fDelta);

// translate along orthonormal axis .... world or local
void glFrameTranslateWorld(GLFrame *frame, float x, float y, float z);
void glFrameTranslateLocal(GLFrame *frame, float x, float y, float z);

// Just assemble the matrix
void glFrameGetMatrix(GLFrame *frame, M3DMatrix44f matrix, bool bRotationOnly);

//assemble the camera matrix
void glFrameGetCameraMatrix(GLFrame *frame, M3DMatrix44f m, bool bRotationOnly);

// rotate around local Y, Z, X
void glFrameRotateLocalY(GLFrame *frame, float fAngle);
void glFrameRotateLocalZ(GLFrame *frame, float fAngle);
void glFrameRotateLocalX(GLFrame *frame, float fAngle);


// Reset axes to make sure they are orthonormal. This should be called on occasion
// if the matrix is long-lived and frequently transformed.
void glFrameNormalize(GLFrame *frame);

// rotate in world coordinates...
void glFrameRotateWorld(GLFrame *frame, float fAngle, float x, float y, float z);
void glFrameRotateLocal(GLFrame *frame, float fAngle, float x, float y, float z);

// Convert Coordinate Systems
// This is pretty much, do the transformation represented by the rotation
// and position on the point
// Is it better to stick to the convention that the destination always comes
// first, or use the conventions that "sounds" like the function...
void glFrameLocalToWorld(GLFrame *frame, const M3DVector3f vLocal, M3DVector3f vWorld, bool bRotOnly);
// Change world coordinates into "local" coordinates
void glFrameWorldToLocal(GLFrame *frame, const M3DVector3f vWorld, M3DVector3f vLocal);
/////////////////////////////////////////////////////////////////////////////
// Transform a point by frame matrix
void glFrameTransformPoint(GLFrame *frame, M3DVector3f vPointSrc, M3DVector3f vPointDst);

void glFrameRotateVector(GLFrame *frame, M3DVector3f vVectorSrc, M3DVector3f vVectorDst);









#endif
