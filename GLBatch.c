/*
GLBatch.c

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright Todd V. Rovito 2013
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include "GLBatch.h"

//////////////////////// TEMPORARY TEMPORARY TEMPORARY - On SnowLeopard this is suppored, but GLEW doens't hook up properly
//////////////////////// Fixed probably in 10.6.3
//#ifdef __APPLE__
//#define glGenVertexArrays glGenVertexArraysAPPLE
//#define glDeleteVertexArrays  glDeleteVertexArraysAPPLE
//#define glBindVertexArray	glBindVertexArrayAPPLE
//#endif

/////////////////////// OpenGL ES support on iPhone/iPad
#ifdef OPENGL_ES
#define GL_WRITE_ONLY   GL_WRITE_ONLY_OES
#define glMapBuffer     glMapBufferOES
#define glUnmapBuffer   glUnmapBufferOES
#endif


#define FALSE 0
#define TRUE 1

void glBatchInitBatch(GLBatch *batch) {
    batch->nNumTextureUnits = 0;
    batch->nNumVerts = 0;
    batch->pVerts = NULL;
    batch->pNormals = NULL;
    batch->pColors = NULL;
    batch->pTexCoords = NULL;
    batch->uiVertexArray = 0;
    batch->uiNormalArray = 0;
    batch->uiColorArray = 0;
    batch->vertexArrayObject = 0;
    batch->bBatchDone = FALSE;
    batch->nVertsBuilding = 0;
    batch->uiTextureCoordArray = NULL;
}

void glBatchBegin(GLBatch *batch, GLenum primitive, GLuint nVerts, GLuint nTextureUnits) {
    
    batch->primitiveType = primitive;
    batch->nNumVerts = nVerts;
    
    if(nTextureUnits > 4)   // Limit to four texture units
        nTextureUnits = 4;
    
    batch->nNumTextureUnits = nTextureUnits;
    
    if(batch->nNumTextureUnits != 0) {
        batch->uiTextureCoordArray = malloc(sizeof(GLuint) * batch->nNumTextureUnits);
        
        // An array of pointers to texture coordinate arrays
        batch->pTexCoords = malloc(sizeof(M3DVector2f) * batch->nNumTextureUnits);
        for(unsigned int i = 0; i < batch->nNumTextureUnits; i++) {
            batch->uiTextureCoordArray[i] = 0;
            batch->pTexCoords[i] = NULL;
        }
    }
    
    // Vertex Array object for this Array
#ifndef OPENGL_ES
    glGenVertexArrays(1, &batch->vertexArrayObject);
    glBindVertexArray(batch->vertexArrayObject);
#endif
}

void glBatchCopyVertexData3f(GLBatch *batch, M3DVector3f *vVerts) {
    
    // First time, create the buffer object, allocate the space
    if(batch->uiVertexArray == 0) {
        glGenBuffers(1, &batch->uiVertexArray);
        glBindBuffer(GL_ARRAY_BUFFER, batch->uiVertexArray);
        glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * batch->nNumVerts, vVerts, GL_DYNAMIC_DRAW);
    }
    else	{ // Just bind to existing object
        glBindBuffer(GL_ARRAY_BUFFER, batch->uiVertexArray);
        
        // Copy the data in
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 3 * batch->nNumVerts, vVerts);
        batch->pVerts = NULL;
    }
}

void glBatchEnd(GLBatch *batch) {
    // Bind everything up in a little package
#ifndef OPENGL_ES
    // Check to see if items have been added one at a time
    if(batch->pVerts != NULL) {
        glBindBuffer(GL_ARRAY_BUFFER, batch->uiVertexArray);
        glUnmapBuffer(GL_ARRAY_BUFFER);
        batch->pVerts = NULL;
    }
    
    if(batch->pColors != NULL) {
        glBindBuffer(GL_ARRAY_BUFFER, batch->uiColorArray);
        glUnmapBuffer(GL_ARRAY_BUFFER);
        batch->pColors = NULL;
    }
    
    if(batch->pNormals != NULL) {
        glBindBuffer(GL_ARRAY_BUFFER, batch->uiNormalArray);
        glUnmapBuffer(GL_ARRAY_BUFFER);
        batch->pNormals = NULL;
    }
    
    for(unsigned int i = 0; i < batch->nNumTextureUnits; i++)
        if(batch->pTexCoords[i] != NULL) {
            glBindBuffer(GL_ARRAY_BUFFER, batch->uiTextureCoordArray[i]);
            glUnmapBuffer(GL_ARRAY_BUFFER);
            batch->pTexCoords[i] = NULL;
        }
    
    // Set up the vertex array object
    glBindVertexArray(batch->vertexArrayObject);
#endif
    
    if(batch->uiVertexArray !=0) {
        glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, batch->uiVertexArray);
        glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }
    
    if(batch->uiColorArray != 0) {
        glEnableVertexAttribArray(GLT_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, batch->uiColorArray);
        glVertexAttribPointer(GLT_ATTRIBUTE_COLOR, 4, GL_FLOAT, GL_FALSE, 0, 0);
    }
    
    if(batch->uiNormalArray != 0) {
        glEnableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, batch->uiNormalArray);
        glVertexAttribPointer(GLT_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }
    
    // How many texture units
    for(unsigned int i = 0; i < batch->nNumTextureUnits; i++)
        if(batch->uiTextureCoordArray[i] != 0) {
            glEnableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0 + i),
            glBindBuffer(GL_ARRAY_BUFFER, batch->uiTextureCoordArray[i]);
            glVertexAttribPointer(GLT_ATTRIBUTE_TEXTURE0 + i, 2, GL_FLOAT, GL_FALSE, 0, 0);
        }
    
    batch->bBatchDone = TRUE;
#ifndef OPENGL_ES
    glBindVertexArray(0);
#endif
    
}

void glBatchDraw(GLBatch *batch) {
    if(!batch->bBatchDone)
		return;
    
#ifndef OPENGL_ES
	// Set up the vertex array object
	glBindVertexArray(batch->vertexArrayObject);
#else
    if(batch->uiVertexArray !=0) {
        glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, batch->uiVertexArray);
        glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }
    
    if(batch->uiColorArray != 0) {
        glEnableVertexAttribArray(GLT_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, batch->uiColorArray);
        glVertexAttribPointer(GLT_ATTRIBUTE_COLOR, 4, GL_FLOAT, GL_FALSE, 0, 0);
    }
    
    if(batch->uiNormalArray != 0) {
        glEnableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, batch->uiNormalArray);
        glVertexAttribPointer(GLT_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
    }
    
    // How many texture units
    for(unsigned int i = 0; i < batch->nNumTextureUnits; i++)
        if(batch->uiTextureCoordArray[i] != 0) {
            glEnableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0 + i),
            glBindBuffer(GL_ARRAY_BUFFER, batch->uiTextureCoordArray[i]);
            glVertexAttribPointer(GLT_ATTRIBUTE_TEXTURE0 + i, 2, GL_FLOAT, GL_FALSE, 0, 0);
        }
#endif
    
    
	glDrawArrays(batch->primitiveType, 0, batch->nNumVerts);
	
#ifndef OPENGL_ES
	glBindVertexArray(0);
#else
    glDisableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
    glDisableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
    glDisableVertexAttribArray(GLT_ATTRIBUTE_COLOR);
    
    for(unsigned int i = 0; i < batch->nNumTextureUnits; i++)
        if(batch->uiTextureCoordArray[i] != 0)
            glDisableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0 + i);
    
#endif
}

void glBatchVertex3f(GLBatch *batch, float x, float y, float z) {
	// First see if the vertex array buffer has been created...
	if(batch->uiVertexArray == 0) {	// Nope, we need to create it
		glGenBuffers(1, &batch->uiVertexArray);
		glBindBuffer(GL_ARRAY_BUFFER, batch->uiVertexArray);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * batch->nNumVerts, NULL, GL_DYNAMIC_DRAW);
    }
    
	// Now see if it's already mapped, if not, map it
	if(batch->pVerts == NULL) {
		glBindBuffer(GL_ARRAY_BUFFER, batch->uiVertexArray);
		batch->pVerts = (M3DVector3f *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    }
    
	// Ignore if we go past the end, keeps things from blowing up
	if(batch->nVertsBuilding >= batch->nNumVerts)
		return;
	
	// Copy it in...
	batch->pVerts[batch->nVertsBuilding][0] = x;
	batch->pVerts[batch->nVertsBuilding][1] = y;
	batch->pVerts[batch->nVertsBuilding][2] = z;
	batch->nVertsBuilding++;
}

// Unlike normal OpenGL immediate mode, you must specify a normal per vertex
// or you will get junk...
void glBatchNormal3f(GLBatch *batch, GLfloat x, GLfloat y, GLfloat z)
{
	// First see if the vertex array buffer has been created...
	if(batch->uiNormalArray == 0) {	// Nope, we need to create it
		glGenBuffers(1, &batch->uiNormalArray);
		glBindBuffer(GL_ARRAY_BUFFER, batch->uiNormalArray);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * batch->nNumVerts, NULL, GL_DYNAMIC_DRAW);
    }
	
	// Now see if it's already mapped, if not, map it
	if(batch->pNormals == NULL) {
		glBindBuffer(GL_ARRAY_BUFFER, batch->uiNormalArray);
		batch->pNormals = (M3DVector3f *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    }
	
	// Ignore if we go past the end, keeps things from blowing up
	if(batch->nVertsBuilding >= batch->nNumVerts)
		return;
	
	// Copy it in...
	batch->pNormals[batch->nVertsBuilding][0] = x;
	batch->pNormals[batch->nVertsBuilding][1] = y;
	batch->pNormals[batch->nVertsBuilding][2] = z;
}

// Unlike normal OpenGL immediate mode, you must specify a texture coord
// per vertex or you will get junk...
void glBatchMultiTexCoord2f(GLBatch *batch, GLuint texture, GLclampf s, GLclampf t)
{
	// First see if the vertex array buffer has been created...
	if(batch->uiTextureCoordArray[texture] == 0) {	// Nope, we need to create it
		glGenBuffers(1, &batch->uiTextureCoordArray[texture]);
		glBindBuffer(GL_ARRAY_BUFFER, batch->uiTextureCoordArray[texture]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 2 * batch->nNumVerts, NULL, GL_DYNAMIC_DRAW);
	}
	
	// Now see if it's already mapped, if not, map it
	if(batch->pTexCoords[texture] == NULL) {
		glBindBuffer(GL_ARRAY_BUFFER, batch->uiTextureCoordArray[texture]);
		batch->pTexCoords[texture] = (M3DVector2f *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    }
	
	// Ignore if we go past the end, keeps things from blowing up
	if(batch->nVertsBuilding >= batch->nNumVerts)
		return;
	
	// Copy it in...
	batch->pTexCoords[texture][batch->nVertsBuilding][0] = s;
	batch->pTexCoords[texture][batch->nVertsBuilding][1] = t;
}

void glBatchDestruct(GLBatch *batch) {
    if (batch->uiVertexArray != 0)
        glDeleteBuffers(1, &batch->uiVertexArray);
    if (batch->uiNormalArray != 0)
        glDeleteBuffers(1, &batch->uiNormalArray);
    if (batch->uiColorArray != 0)
        glDeleteBuffers(1, &batch->uiColorArray);
    for (unsigned int i = 0; i < batch->nNumTextureUnits; i++) {
        glDeleteBuffers(1, &batch->uiTextureCoordArray[i]);
    }

#ifndef OPENGL_ES
    glDeleteVertexArrays(1, &batch->vertexArrayObject);
#endif

    if (batch->uiTextureCoordArray)
        free(batch->uiTextureCoordArray);
    if (batch->pTexCoords)
        free(batch->pTexCoords);
}



