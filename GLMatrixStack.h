/*
GLMatrixStack.h

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright (c) 2013, Todd V. Rovito
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef GLMatrixStack_h
#define GLMatrixStack_h

#include "GLTools.h"
#include "math3d.h"
#include "GLFrame.h"

enum GLT_STACK_ERROR { GLT_STACK_NOERROR = 0, GLT_STACK_OVERFLOW, GLT_STACK_UNDERFLOW };

struct GLMatrixStackStruct {
    enum GLT_STACK_ERROR lastError;
    int stackDepth;
    int stackPointer;
    M3DMatrix44f *pStack;
};

typedef struct GLMatrixStackStruct GLMatrixStack;

void glMatrixStackInit(GLMatrixStack *matrixStack, int iStackDepth);
void glMatrixStackDestroy(GLMatrixStack *matrixStack);
void glMatrixStackLoadIdentity(GLMatrixStack *matrixStack);
void glMatrixStackLoadMatrix(GLMatrixStack *matrixStack, const M3DMatrix44f mMatrix);
void glMatrixStackLoadMatrixFromFrame(GLMatrixStack *matrixStack, GLFrame *frame);
void glMatrixStackMultiplyMatrix(GLMatrixStack *matrixStack, const M3DMatrix44f mMatrix);
void glMatrixStackMultiplyMatrixFromFrame(GLMatrixStack *matrixStack, GLFrame *frame);
void glMatrixStackPushMatrix(GLMatrixStack *matrixStack);
void glMatrixStackPopMatrix(GLMatrixStack *matrixStack);
void glMatrixStackScale(GLMatrixStack *matrixStack, float x, float y, float z);
void glMatrixStackTranslate(GLMatrixStack *matrixStack, float x, float y, float z);
void glMatrixStackRotate(GLMatrixStack *matrixStack, float angle, float x, float y, float z);
void glMatrixStackScaleVector(GLMatrixStack *matrixStack, const M3DVector3f vScale);
void glMatrixStackTranslateVector(GLMatrixStack *matrixStack, const M3DVector3f vTranslate);
void glMatrixStackRotateVector(GLMatrixStack *matrixStack, float angle, M3DVector3f vAxis);
void glMatrixStackPushNewMatrix(GLMatrixStack *matrixStack, const M3DMatrix44f mMatrix);
void glMatrixStackPushNewMatrixFromFrame(GLMatrixStack *matrixStack, GLFrame *frame);
M3DMatrix44f *glMatrixStackGetMatrix(GLMatrixStack *matrixStack);
void glMatrixStackGetMatrixCopy(GLMatrixStack *matrixStack, M3DMatrix44f mMatrix);
enum GLT_STACK_ERROR glMatrixStackGetLastError(GLMatrixStack *matrixStack);

#endif
