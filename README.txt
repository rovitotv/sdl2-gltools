About
=====

SDL2-GLTools is a port to C of the Open GL Super Bible's GLTools.  The reason
for the port is to make it easier to use GLTools with higher level languages
such as Python.  SDL is used to get a OpenGL context instead of GLU because
the keyboard and mouse handling offers better response.  This project can
be combined with py-sdl2 if you want to use other parts of SDL.  Example
programs are provided for both C and Python.

For the Python bindings SDL2-GLTools support ctypes (in the ctypes directory)
and Cython (in the cython directory).  After experimenting with Cython this
will become the perfered and supported Python bindings.  It gives users more
options and allows developers to build a standalone executable without Python.
Cython does have some overhead but it works really well at integrating Python
with C.

Installation
============

SDL2-GLTools requires SDL2 the version that is expected is version 2.0.1.

1) Download the SDL2 version 2.0.1 tarball and unpack somewhere
2) ./configure --prefix=/Volumes/SecureThreeB/SDL2-2.0.1
3) make
4) make install

Also need SDL_mixer 2.0
1) Download the SDL2_mixer-2.0.0 tarball and unpack somewhere
2) ./configure --prefix=/Volumes/SecureThreeB/SDL2-2.0.1
3) make
4) make install

Then install PySDL2 with the following commands:
cd /Volumes/SecureThreeB/PySDL2-dev
/Volumes/SecureThreeB/python/bin/python3.3 setup.py install

don't forget to set the path to the SDL library for Python programs to work
correctly:
    export PYSDL2_DLL_PATH=/Volumes/SecureThreeB/SDL2-2.0.1/lib/

Then you build SDL2-GLTools the normal CMake way with either XCode or make
files.  Make sure the CMakeLists.txt file points to the correct SDL2-2.0.1
library.  Instead of changing the values in the CMakeLists.txt file use
the CMakeGUI and set the CMAKE_C_FLAGS and CMAKE_EXE_LINKER_FLAGS 
typical values for my Mac are:
CMAKE_C_FLAGS = -I/Volumes/SecureNTMTOCython/source/SDL2-GLTools/ -I/Volumes/SecureNTMTOCython/tools/include/SDL2 -I/usr/X11R6/include -D_THREAD_SAFE
CMAKE_EXE_LINKER_FLAGS =  -L/Volumes/SecureNTMTOCython/tools/lib -lSDL2

You can obtain the SDL values by running sdl2-config.


License
=======

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright Todd V. Rovito 2013
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Links
=====

SDL	http://libsdl.org
py-sdl2 https://bitbucket.org/rovitotv/py-sdl2
python http://python.org

To-Do
=====

*Figure out how to build a cube
*Fix glBatchCopyVertexData3f in GLTools.py the arguments are not right, it only works
for Triangle.py test case
*Write better installation directions
