# setup.py for SDL2-GLTools please compile the library seperatly using cmake && make && make install
from distutils.core import setup

setup(name='GLTools',
      version='0.1',
      description='OpenGL Tools for Python C++ converted to C',
      author='Todd V. Rovito',
      author_email='rovitotv@gmail.com',
      url='https://bitbucket.org/rovitotv/sdl2-gltools',
      py_modules=['GLTools'],
     )
