# SphereWorld.py
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See LICENSE.txt for full details of the MPL license.
#
# Copyright Todd V. Rovito 2013
# https://bitbucket.org/rovitotv/sdl2-gltools
#
# This is a Python version of SphereWorld.c.  It use Py SDL2 and PyGLTools via
# ctypes.  This example will draw a floor of green lines and a spinning red
# torus.

# We'll use sys to properly exit with an error code.
#
# don't forget to set the path to the SDL library:
# export PYSDL2_DLL_PATH=/Volumes/SecureThreeB/SDL2/lib/
import sys
from ctypes import *
import pdb
from GLTools import *

# Try to import the video system. Since sdl2.ext makes use of sdl2, the
# import might fail, if the SDL DLL could not be loaded. In that case, just
# print the error and exit with a proper error code.
try:
    from sdl2 import *
except ImportError:
    import traceback
    traceback.print_exc()
    sys.exit(1)

def sdldie(error):
    print("ERROR: %s SDL Error: %s" % (error, SDL_GetError()))
    sys.exit(1)

def frange(start, end=None, inc=None):
    "A range function, that does accept float increments..."
    
    if end == None:
        end = start + 0.0
        start = 0.0
    
    if inc == None:
        inc = 1.0
    
    L = []
    while 1:
        next = start + len(L) * inc
        if inc > 0 and next >= end:
            break
        elif inc < 0 and next <= end:
            break
        L.append(next)
    
    return L

def SetupRC(uiStockShaders, floorBatch, torusBatch):
    glShaderManagerInitializeStockShaders(uiStockShaders)
    glToolsEnableDepthTest()
    glToolsPolygonMode()
    glToolsSetClearColor(0.0, 0.0, 0.0, 1.0)
    glToolsMakeTorus(torusBatch, 0.4, 0.15, 30, 30	);
    glBatchBegin(floorBatch, GL_LINES, 324, 0)
    for x in frange(-20.0, 20.0, 0.5):
        glBatchVertex3f(floorBatch, x, -0.55, 20.0)
        glBatchVertex3f(floorBatch, x, -0.55, -20.0)
        glBatchVertex3f(floorBatch, 20.0, -0.55, x)
        glBatchVertex3f(floorBatch, -20.0, -0.55, x)
    
    glBatchEnd(floorBatch)

def ChangeSize(nWidth, nHeight, viewFrustum, projectionMatrix, modelViewMatrix, transformPipeline):
    glFrustumSetPerspective(viewFrustum, 35.0, float(nWidth)/ float(nHeight),
            1.0, 100.0)
    # default return type is a int we have to fix this!  See CTypes documentation or change the API.
    
    glMatrixStackLoadMatrix(projectionMatrix, glFrustumGetProjectionMatrix(viewFrustum))
    glGeometryTransformSetMatrixStacks(transformPipeline, modelViewMatrix, projectionMatrix)

def RenderScene(rotTimer, modelViewMatrix, uiStockShaders, transformPipeline, floorBatch, torusBatch):
    vFloorColor = vColor(0.0, 1.0, 0.0, 1.0)
    vTorusColor = vColor(1.0, 0.0, 0.0, 1.0)
    # time based animation
    yRot = stopWatchGetElapsedSeconds(rotTimer) * 60.0
    glToolsClearColorDepthBuffer()
    glMatrixStackPushMatrix(modelViewMatrix)
    # Draw the ground
    glShaderManagerUseStockShaderFlat(uiStockShaders,
            glGeometryTransformGetModelViewProjectionMatrix(transformPipeline), vFloorColor)
    glBatchDraw(floorBatch)
    # draw the spinning torus
    glMatrixStackTranslate(modelViewMatrix, 0.0, 0.0, -2.5)
    glMatrixStackRotate(modelViewMatrix, yRot, 0.0, 1.0, 0.0)
    glShaderManagerUseStockShaderFlat(uiStockShaders,
       glGeometryTransformGetModelViewProjectionMatrix(transformPipeline), vTorusColor)
    glTriangleBatchDraw(torusBatch)
    glMatrixStackPopMatrix(modelViewMatrix)
    
def main():
    uiStockShaders = uiStockShadersType()
    floorBatch = GLBatch()
    torusBatch = GLTriangle()
    rotTimer = CStopWatch()
    modelViewMatrix = GLMatrixStack()
    transformPipeline = GLGeometryTransform()
    projectionMatrix = GLMatrixStack()
    viewFrustum = GLFrustum()
    
    if SDL_Init(SDL_INIT_VIDEO) != 0:
        sdldie("can't init video")
    # init all the objects that we need
    glBatchInitBatch(floorBatch)
    glTriangleBatchInit(torusBatch)
    stopWatchInit(rotTimer)
    glMatrixStackInit(modelViewMatrix, 64)
    glMatrixStackInit(projectionMatrix, 64)
    glFrustumInitDefault(viewFrustum)


    # request OpenGL 3.2 context
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2)
    # Turn on double buffering with 24 bit z buffer
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1)
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24)
    mainwindow = SDL_CreateWindow(b"SphereWorld.py",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            800, 600, SDL_WINDOW_OPENGL or SDL_WINDOW_SHOWN)
    if not mainwindow:
        sdldie("Unable to create window")
    maincontext = SDL_GL_CreateContext(mainwindow)

    err = GLToolsDll.glewInit()
    if err != 0:
        print("GLEW Error: %s", GLToolsDLL.glewGetErrorString(err));
        sys.exit(1)
    
    SetupRC(uiStockShaders, floorBatch, torusBatch)
    ChangeSize(800, 600, viewFrustum, projectionMatrix, modelViewMatrix, transformPipeline)
    for cycle in range (0, 150):
        RenderScene(rotTimer, modelViewMatrix, uiStockShaders, transformPipeline, floorBatch, torusBatch)
        SDL_GL_SwapWindow(mainwindow)
        SDL_Delay(100)

    # start to tear things down
    SDL_DestroyWindow(mainwindow)
    SDL_GL_DeleteContext(maincontext)
    SDL_Quit()

if __name__ == "__main__":
    main()
