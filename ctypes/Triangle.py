# Triangle.py
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See LICENSE.txt for full details of the MPL license.
#
# Copyright Todd V. Rovito 2013
# https://bitbucket.org/rovitotv/sdl2-gltools
#
# This is a Python version of Triangle.c.  It use Py SDL2 and PyGLTools via
# ctypes.  This example will draw a simple red triangle on the screen with
# a blue background.

# We'll use sys to properly exit with an error code.
#
# don't forget to set the path to the SDL library:
# export PYSDL2_DLL_PATH=/Volumes/SecureThreeB/SDL2/lib/
import sys
from ctypes import *
import pdb
from GLTools import *

# Try to import the video system. Since sdl2.ext makes use of sdl2, the
# import might fail, if the SDL DLL could not be loaded. In that case, just
# print the error and exit with a proper error code.
try:
    from sdl2 import *
except ImportError:
    import traceback
    traceback.print_exc()
    sys.exit(1)

def sdldie(error):
    print("ERROR: %s SDL Error: %s" % (error, SDL_GetError()))
    sys.exit(1)

def SetupRC(uiStockShaders, triangleBatch):
    # blue background
    glToolsSetClearColor(0.0, 0.0, 1.0, 1.0)
    glShaderManagerInitializeStockShaders(uiStockShaders)

    # load up a triangle
    vVertsType = c_float * 9
    vVerts = vVertsType(-0.5, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.5, 0.0)

    glBatchBegin(triangleBatch, GL_TRIANGLES, 3, 0) # GL_TRIANGLES is 0x0004 in gl.h
    glBatchCopyVertexData3f(triangleBatch, vVerts)
    glBatchEnd(triangleBatch)

def RenderScene(uiStockShaders, triangleBatch, GLToolsDll):
    # clear the window with the current clearing color
    glToolsClear()

    # make the red color
    vRedType = c_float * 4
    vRed = vRedType(1.0, 0.0, 0.0, 1.0)
    glShaderManagerUseStockShader(uiStockShaders, GLT_SHADER_IDENTITY, vRed)
    glBatchDraw(triangleBatch)

def main():
    triangleBatch = GLBatch()
    uiStockShaders = uiStockShadersType()
    
    if SDL_Init(SDL_INIT_VIDEO) != 0:
        sdldie("can't init video")
            #GLToolsDll = cdll.LoadLibrary("/Volumes/SecureThreeB/build/SDL2-GLTools/Debug/libGLTools.dylib")
    #GLToolsDll.glBatchInitBatch(byref(triangleBatch))

    glBatchInitBatch(triangleBatch)
    

    # request OpenGL 3.2 context
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2)
    # Turn on double buffering with 24 bit z buffer
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1)
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24)
    mainwindow = SDL_CreateWindow(b"Triangle.py",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            800, 600, SDL_WINDOW_OPENGL or SDL_WINDOW_SHOWN)
    if not mainwindow:
        sdldie("Unable to create window")
    maincontext = SDL_GL_CreateContext(mainwindow)

    err = GLToolsDll.glewInit()
    if err != 0:
        print("GLEW Error: %s", GLToolsDLL.glewGetErrorString(err));
        sys.exit(1)
    
    SetupRC(uiStockShaders, triangleBatch)
    RenderScene(uiStockShaders, triangleBatch, GLToolsDll)
    SDL_GL_SwapWindow(mainwindow)

    SDL_Delay(8000)

    # start to tear things down
    SDL_GL_DeleteContext(maincontext)
    SDL_DestroyWindow(mainwindow)
    SDL_Quit()

if __name__ == "__main__":
    main()
