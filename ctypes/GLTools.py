# GLTools.py
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See LICENSE.txt for full details of the MPL license.
#
# Copyright Todd V. Rovito 2013
# https://bitbucket.org/rovitotv/sdl2-gltools
#
# This file contains all the structures and definitions needed for GLTools
# which makes OpenGL easier and simple.  With this file each GLTools function
# will have a argument list and return list (if needed) which removes the
# requirement to put c_float(argumentName) around each argument
import os
from ctypes import *

## Shared Object loading stuff ************************************************

# at some point we need to improve the dll loading code
GLToolsDllPath = os.getenv("GLTOOLS_DLL_PATH")
GLToolsDllFullPath = GLToolsDllPath + "/libGLTools.dylib"
GLToolsDll = cdll.LoadLibrary(GLToolsDllFullPath)


def attach(funcname, args=None, returns=None, optfunc=None):
    """attach the passed argument and return value types to the specified
        function."""
    func = getattr(GLToolsDll, funcname, None)
    if not func:
        func = optfunc
    func.argtypes = args
    func.restype = returns
    return func


## math3d *********************************************************************
M3DMatrix44f = c_float * 16
M3DMatrix33f = c_float * 9
M3DVector4f = c_float * 4
M3DVector3f = c_float * 3
vColor = c_float * 4


## GLBatch ********************************************************************
class GLBatch(Structure):
    _fields_ = [("primitiveType", c_int),
                ("uiVertexArray", c_uint),
                ("uiNormalArray", c_uint),
                ("uiColorArray", c_uint),
                ("uiTexureCoordArray", POINTER(c_uint)),
                ("vertexArrayObject", c_uint),
                ("nVertsBuilding", c_uint),
                ("nNumVerts", c_uint),
                ("nNumTextureUnits", c_uint),
                ("bBatchDone", c_int),
                ("pVerts", POINTER(c_float)),
                ("pNormals", POINTER(c_float)),
                ("pColors", POINTER(c_float)),
                ("pTexCoords", c_void_p)]
GL_LINES = 0x0001
GL_TRIANGLES = 0x0004

glBatchInitBatch = attach("glBatchInitBatch", [POINTER(GLBatch)])
glBatchBegin = attach("glBatchBegin", [POINTER(GLBatch), c_uint, c_uint,
                                       c_uint])
# this is broken the last argument of function below should be an array of
# M3DVector3f but I need to figure out how to pass an array into the function?
glBatchCopyVertexData3f = attach("glBatchCopyVertexData3f",
                                 [POINTER(GLBatch), c_float * 9])
glBatchEnd = attach("glBatchEnd", [POINTER(GLBatch)])
glBatchDraw = attach("glBatchDraw", [POINTER(GLBatch)])
glBatchVertex3f = attach("glBatchVertex3f", [POINTER(GLBatch), c_float,
                                             c_float, c_float])

## GLShaderManager ************************************************************
GLT_SHADER_LAST = 9  # defined in GLShaderManager.h line 51 in the ENUM
uiStockShadersType = c_uint * GLT_SHADER_LAST
GLT_SHADER_IDENTITY = 0
GLT_SHADER_FLAT = 1

glShaderManagerInitializeStockShaders = attach(
    "glShaderManagerInitializeStockShaders", [c_uint * GLT_SHADER_LAST])
glShaderManagerUseStockShader = attach(
    "glShaderManagerUseStockShader",
    [c_uint * GLT_SHADER_LAST, c_uint, vColor])
glShaderManagerUseStockShaderFlat = attach(
    "glShaderManagerUseStockShaderFlat",
    [c_uint * GLT_SHADER_LAST, POINTER(c_float), vColor])


## GLTriangleBatch ************************************************************
class GLTriangle(Structure):
    _fields_ = [("pIndexes", POINTER(c_ushort)),
                ("pVerts", POINTER(c_float)),
                ("pNorms", POINTER(c_float)),
                ("pTexCoords", POINTER(c_float)),
                ("nMaxIndexes", c_uint),
                ("nNumIndexes", c_uint),
                ("nNumVerts", c_uint),
                ("bufferObjects", c_uint * 4),
                ("vertexArrayBufferObject", c_uint)]

glTriangleBatchInit = attach("glTriangleBatchInit", [POINTER(GLTriangle)])
glTriangleBatchDraw = attach("glTriangleBatchDraw", [POINTER(GLTriangle)])


## GLFrame ********************************************************************
class GLFrame(Structure):
    _fields_ = [("vOrigin", M3DVector3f),
                ("vForward", M3DVector3f),
                ("vUp", M3DVector3f)]

glFrameInit = attach("glFrameInit", [POINTER(GLFrame)])
glFrameMoveForward = attach("glFrameMoveForward", [POINTER(GLFrame), c_float])
glFrameRotateWorld = attach("glFrameRotateWorld", [POINTER(GLFrame), c_float,
                                                   c_float, c_float, c_float])
glFrameGetCameraMatrix = attach("glFrameGetCameraMatrix", [POINTER(GLFrame),
                                M3DMatrix44f, c_bool])
glFrameTranslateWorld = attach("glFrameTranslateWorld", [POINTER(GLFrame),
                               c_float, c_float, c_float])
glFrameSetOriginFromPoints = attach("glFrameSetOriginFromPoints",
                                    [POINTER(GLFrame), c_float, c_float,
                                     c_float])
glFrameSetUpVectorFromPoints = attach("glFrameSetUpVectorFromPoints",
                                      [POINTER(GLFrame), c_float, c_float,
                                       c_float])
glFrameSetForwardVectorFromPoints = attach("glFrameSetForwardVectorFromPoints",
                                           [POINTER(GLFrame), c_float, c_float,
                                            c_float])


## GLMatrixStack **************************************************************
class GLMatrixStack(Structure):
    _fields_ = [("lastError", c_int),
                ("stackDepth", c_int),
                ("stackPointer", c_int),
                ("pStack", POINTER(c_float))]

glMatrixStackInit = attach("glMatrixStackInit",
                           [POINTER(GLMatrixStack), c_int])
glMatrixStackLoadMatrix = attach("glMatrixStackLoadMatrix",
                                 [POINTER(GLMatrixStack), POINTER(c_float)])
glMatrixStackPushMatrix = attach("glMatrixStackPushMatrix",
                                 [POINTER(GLMatrixStack)])
glMatrixStackPushNewMatrix = attach("glMatrixStackPushNewMatrix",
                                    [POINTER(GLMatrixStack), M3DMatrix44f])
glMatrixStackTranslate = attach("glMatrixStackTranslate",
                                [POINTER(GLMatrixStack), c_float, c_float,
                                 c_float])
glMatrixStackRotate = attach("glMatrixStackRotate", [POINTER(GLMatrixStack),
                                                     c_float, c_float, c_float,
                                                     c_float])
glMatrixStackPopMatrix = attach("glMatrixStackPopMatrix",
                                [POINTER(GLMatrixStack)])
glMatrixStackMultiplyMatrixFromFrame = attach(
    "glMatrixStackMultiplyMatrixFromFrame", [POINTER(GLMatrixStack),
                                             POINTER(GLFrame)])


## GLGeometryTransform ********************************************************
class GLGeometryTransform(Structure):
    _fields_ = [("_mModelViewProjection", POINTER(c_float)),
                ("_mNormalMatrix", POINTER(c_float)),
                ("_mModelView", GLMatrixStack),
                ("_mProjection", GLMatrixStack)]

glGeometryTransformSetMatrixStacks = attach(
    "glGeometryTransformSetMatrixStacks", [POINTER(GLGeometryTransform),
                                           POINTER(GLMatrixStack),
                                           POINTER(GLMatrixStack)])
glGeometryTransformGetModelViewProjectionMatrix = attach(
    "glGeometryTransformGetModelViewProjectionMatrix",
    [POINTER(GLGeometryTransform)], POINTER(c_float))


## GLFrustum ******************************************************************
class GLFrustum(Structure):
    _fields_ = [("projMatrix", M3DMatrix44f),
                ("nearUL", M3DVector4f),
                ("nearLL", M3DVector4f),
                ("nearUR", M3DVector4f),
                ("nearLR", M3DVector4f),
                ("farUL", M3DVector4f),
                ("farLL", M3DVector4f),
                ("farUR", M3DVector4f),
                ("farLR", M3DVector4f),
                ("nearULT", M3DVector4f),
                ("nearLLT", M3DVector4f),
                ("nearURT", M3DVector4f),
                ("nearLRT", M3DVector4f),
                ("farULT", M3DVector4f),
                ("farLLT", M3DVector4f),
                ("farURT", M3DVector4f),
                ("farLRT", M3DVector4f),
                ("nearPlane", M3DVector4f),
                ("farPlane", M3DVector4f),
                ("leftPlane", M3DVector4f),
                ("rightPlane", M3DVector4f),
                ("topPlane", M3DVector4f),
                ("bottomPlane", M3DVector4f)]

glFrustumInitDefault = attach("glFrustumInitDefault", [POINTER(GLFrustum)])
glFrustumSetPerspective = attach(
    "glFrustumSetPerspective",
    [POINTER(GLFrustum), c_float, c_float, c_float, c_float])
glFrustumGetProjectionMatrix = attach("glFrustumGetProjectionMatrix",
                                      [POINTER(GLFrustum)], POINTER(c_float))

## GLTools ********************************************************************
glToolsSetClearColor = attach("glToolsSetClearColor",
                              [c_float, c_float, c_float, c_float])
glToolsClearColorDepthBuffer = attach("glToolsClearColorDepthBuffer")
glToolsClear = attach("glToolsClear")
glToolsEnableDepthTest = attach("glToolsEnableDepthTest")
glToolsPolygonMode = attach("glToolsPolygonMode")
glToolsMakeTorus = attach("glToolsMakeTorus", [POINTER(GLTriangle), c_float,
                                               c_float, c_int, c_int])
glToolsMakeSphere = attach("glToolsMakeSphere", [POINTER(GLTriangle), c_float,
                                                 c_int, c_int])
glToolsMakeCube = attach("glToolsMakeCube", [POINTER(GLBatch), c_float])


## StopWatch ******************************************************************
class timeval(Structure):
    _fields_ = [("tv_sec", c_long),
                ("tv_usec", c_long)]


class CStopWatch(Structure):
    _fields_ = [("m_LastCount", timeval)]

stopWatchInit = attach("stopWatchInit", [POINTER(CStopWatch)])
stopWatchGetElapsedSeconds = attach("stopWatchGetElapsedSeconds",
                                    [POINTER(CStopWatch)], c_float)
