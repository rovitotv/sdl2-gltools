/*
GLMatrixStack.c

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright Todd V. Rovito 2013
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include "GLMatrixStack.h"

void glMatrixStackInit(GLMatrixStack *matrixStack, int iStackDepth) {
    matrixStack->stackDepth = iStackDepth;
    matrixStack->pStack = malloc(sizeof(M3DMatrix44f) * iStackDepth);
    matrixStack->stackPointer = 0;
    m3dLoadIdentity44f(matrixStack->pStack[0]);
    matrixStack->lastError = GLT_STACK_NOERROR;
}

void glMatrixStackDestroy(GLMatrixStack *matrixStack) {
    free(matrixStack->pStack);
    matrixStack->stackDepth = 0;
}

void glMatrixStackLoadIdentity(GLMatrixStack *matrixStack) {
    m3dLoadIdentity44f(matrixStack->pStack[matrixStack->stackPointer]);
}

void glMatrixStackLoadMatrix(GLMatrixStack *matrixStack, const M3DMatrix44f mMatrix) {
    m3dCopyMatrix44f(matrixStack->pStack[matrixStack->stackPointer], mMatrix);
}

void glMatrixStackLoadMatrixFromFrame(GLMatrixStack *matrixStack, GLFrame *frame) {
    M3DMatrix44f m;
    glFrameGetMatrix(frame, m, false);
    glMatrixStackLoadMatrix(matrixStack, m);
}

void glMatrixStackMultiplyMatrix(GLMatrixStack *matrixStack, const M3DMatrix44f mMatrix) {
    M3DMatrix44f mTemp;
    m3dCopyMatrix44f(mTemp, matrixStack->pStack[matrixStack->stackPointer]);
    m3dMatrixMultiply44f(matrixStack->pStack[matrixStack->stackPointer], mTemp, mMatrix);
}

void glMatrixStackMultiplyMatrixFromFrame(GLMatrixStack *matrixStack, GLFrame *frame) {
    M3DMatrix44f m;
    glFrameGetMatrix(frame, m, false);
    glMatrixStackMultiplyMatrix(matrixStack, m);
}

void glMatrixStackPushMatrix(GLMatrixStack *matrixStack) {
    if (matrixStack->stackPointer < matrixStack->stackDepth) {
        matrixStack->stackPointer++;
        m3dCopyMatrix44f(matrixStack->pStack[matrixStack->stackPointer],
                         matrixStack->pStack[matrixStack->stackPointer-1]);
    } else {
        matrixStack->lastError = GLT_STACK_OVERFLOW;
    }
}

void glMatrixStackPopMatrix(GLMatrixStack *matrixStack) {
    if (matrixStack->stackPointer > 0) {
        matrixStack->stackPointer--;
    } else {
        matrixStack->lastError = GLT_STACK_UNDERFLOW;
    }
}

void glMatrixStackScale(GLMatrixStack *matrixStack, float x, float y, float z) {
    M3DMatrix44f mTemp, mScale;
    m3dScaleMatrix44f(mScale, x, y, z);
    m3dCopyMatrix44f(mTemp, matrixStack->pStack[matrixStack->stackPointer]);
    m3dMatrixMultiply44f(matrixStack->pStack[matrixStack->stackPointer], mTemp, mScale);
}

void glMatrixStackTranslate(GLMatrixStack *matrixStack, float x, float y, float z) {
    M3DMatrix44f mTemp, mScale;
    m3dTranslationMatrix44f(mScale, x, y, z);
    m3dCopyMatrix44f(mTemp, matrixStack->pStack[matrixStack->stackPointer]);
    m3dMatrixMultiply44f(matrixStack->pStack[matrixStack->stackPointer],
                         mTemp, mScale);
}

void glMatrixStackRotate(GLMatrixStack *matrixStack, float angle, float x, float y, float z) {
    M3DMatrix44f mTemp, mRotate;
    m3dRotationMatrix44f(mRotate, (float)m3dDegToRad(angle), x, y, z);
    m3dCopyMatrix44f(mTemp, matrixStack->pStack[matrixStack->stackPointer]);
    m3dMatrixMultiply44f(matrixStack->pStack[matrixStack->stackPointer], mTemp, mRotate);
}

void glMatrixStackScaleVector(GLMatrixStack *matrixStack, const M3DVector3f vScale) {
    M3DMatrix44f mTemp, mScale;
    m3dScaleMatrix44Vectorf(mScale, vScale);
    m3dCopyMatrix44f(mTemp, matrixStack->pStack[matrixStack->stackPointer]);
    m3dMatrixMultiply44f(matrixStack->pStack[matrixStack->stackPointer], mTemp, mScale);
}

void glMatrixStackTranslateVector(GLMatrixStack *matrixStack, const M3DVector3f vTranslate) {
    M3DMatrix44f mTemp, mTranslate;
    m3dLoadIdentity44f(mTranslate);
    m3dSetMatrixColumn44f(mTranslate, vTranslate, 3);
    m3dCopyMatrix44f(mTemp, matrixStack->pStack[matrixStack->stackPointer]);
    m3dMatrixMultiply44f(matrixStack->pStack[matrixStack->stackPointer], mTemp, mTranslate);
}

void glMatrixStackRotateVector(GLMatrixStack *matrixStack, float angle, M3DVector3f vAxis) {
    M3DMatrix44f mTemp, mRotation;
    m3dRotationMatrix44f(mRotation, (float)m3dDegToRad(angle), vAxis[0], vAxis[1], vAxis[2]);
    m3dCopyMatrix44f(mTemp, matrixStack->pStack[matrixStack->stackPointer]);
    m3dMatrixMultiply44f(matrixStack->pStack[matrixStack->stackPointer], mTemp, mRotation);
}

void glMatrixStackPushNewMatrix(GLMatrixStack *matrixStack, const M3DMatrix44f mMatrix) {
    if(matrixStack->stackPointer < matrixStack->stackDepth) {
        matrixStack->stackPointer++;
        m3dCopyMatrix44f(matrixStack->pStack[matrixStack->stackPointer], mMatrix);
    }
    else {
        matrixStack->lastError = GLT_STACK_OVERFLOW;
    }
}

void glMatrixStackPushNewMatrixFromFrame(GLMatrixStack *matrixStack, GLFrame *frame) {
    M3DMatrix44f m;
    glFrameGetMatrix(frame, m, false);
    glMatrixStackPushNewMatrix(matrixStack, m);
}

M3DMatrix44f *glMatrixStackGetMatrix(GLMatrixStack *matrixStack) {
    return &matrixStack->pStack[matrixStack->stackPointer];
}

void glMatrixStackGetMatrixCopy(GLMatrixStack *matrixStack, M3DMatrix44f mMatrix) {
    m3dCopyMatrix44f(mMatrix, matrixStack->pStack[matrixStack->stackPointer]);
}

enum GLT_STACK_ERROR glMatrixStackGetLastError(GLMatrixStack *matrixStack) {
    enum GLT_STACK_ERROR returnValue = matrixStack->lastError;
    matrixStack->lastError = GLT_STACK_NOERROR;
    return returnValue;
}









