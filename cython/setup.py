# don't forget to export CFLAGS and LDFLAGS with the following bash commands
#   export CFLAGS=`sdl2-config --cflags`
#   export LDFLAGS=`sdl2-config --static-libs`
# then you have to add on stuff for GLTools
# export CFLAGS="-I/Volumes/SecureNTMTOCython/tools/include/SDL2 -I/usr/X11R6/include -D_THREAD_SAFE -I/Volumes/SecureNTMTOCython/source/SDL2-GLTools"
# export LDFLAGS="-L/Volumes/SecureNTMTOCython/tools/lib -lSDL2 -lm -liconv -Wl,-framework,OpenGL -Wl,-framework,ForceFeedback -lobjc -Wl,-framework,Cocoa -Wl,-framework,Carbon -Wl,-framework,IOKit -Wl,-framework,CoreAudio -Wl,-framework,AudioToolbox -Wl,-framework,AudioUnit -L/Volumes/SecureNTMTOCython/builds/SDL2-GLTools -lGLTools"
# to build use the following command:
#    python3 setup.py build_ext -i
# I guess to install into CPython's path use the command (untested):
#   python3 setup.py install
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import sys
import subprocess

args = sys.argv[1:]

if "cleanall" in args:
    print("Deleting cython files...")
    # Just in case the build directory was created by accident,
    # note that shell=True should be ok here because the command is constant
    subprocess.Popen("rm -rf build", shell=True, executable="/bin/bash")
    subprocess.Popen("rm -rf *.c", shell=True, executable="/bin/bash")
    subprocess.Popen("rm -rf *.so", shell=True, executable="/bin/bash")

    # now do a normal clean
    sys.argv[1] = "clean"

setup(
    cmdclass={'build_ext': build_ext},
    ext_modules=[Extension("SDL", ["SDL.pyx"], libraries=["SDL2"])]
)

setup(
    cmdclass={'build_ext': build_ext},
    ext_modules=[Extension("GLTools", ["GLTools.pyx"], libraries=["GLTools"])]
)
