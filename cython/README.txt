I wanted to experiment with Cython more and see if I like it better than
ctypes.  The code in this directory is all Cython releated.  To start
make sure the SDL2-GLTools library is completly built because Cython
will need that in order to run the examples.  SDL2 must be installed
as well.

One trick to debug.  Use -ggdb flag in CFLAGS then:
"gdb -args python3 Triangle.py"

run
then bt 20 for the last 20 calls in the back trace to find out where
program is crashing.
