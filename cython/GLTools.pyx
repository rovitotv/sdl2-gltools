cimport CGLTools
#cimport StopWatch
cimport cython
#from libc.stdlib cimport malloc, free
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free

GL_TRIANGLES = 4
GL_LINES = 0x0001
GLT_SHADER_IDENTITY = CGLTools.GLT_SHADER_IDENTITY



cdef class GLShaderManager:
    cdef CGLTools.GLuint* _uiStockShaders
    def __cinit__(self):
        self._uiStockShaders = <CGLTools.GLuint *>PyMem_Malloc(50*cython.sizeof(CGLTools.GLuint))
        if not self._uiStockShaders:
            raise MemoryError()
        CGLTools.glShaderManagerInitializeStockShaders(self._uiStockShaders)

    def __dealloc__(self):
        if self._uiStockShaders is not NULL:
            CGLTools.glShaderManagerDestruct(self._uiStockShaders)
            PyMem_Free(self._uiStockShaders)
            self._uiStockShaders = NULL

    def useStockShader(self, CGLTools.GLenum shader, vColor):
        # vRed is actually a Python array so we need to copy it to a c array
        # then pass to glShaderManagerUseStockShader after that we free cFloats
        cdef float * cFloats
        cdef int i

        cFloats = <float *>PyMem_Malloc(len(vColor)*cython.sizeof(float))
        if cFloats is NULL:
            raise MemoryError()
        for i in xrange(len(vColor)):
            cFloats[i] = vColor[i]
        CGLTools.glShaderManagerUseStockShader(self._uiStockShaders, GLT_SHADER_IDENTITY, cFloats)
        PyMem_Free(cFloats)


cdef class GLBatch:
    cdef CGLTools.GLBatch* _glBatch
    def __cinit__(self):
        self._glBatch = <CGLTools.GLBatch *>PyMem_Malloc(cython.sizeof(CGLTools.GLBatch))
        if not self._glBatch:
            raise MemoryError()
        CGLTools.glBatchInitBatch(self._glBatch)

    def __dealloc__(self):
        if self._glBatch is not NULL:
            CGLTools.glBatchDestruct(self._glBatch)
            PyMem_Free(self._glBatch)
            self._glBatch = NULL

    def begin(self, CGLTools.GLenum primitive, CGLTools.GLuint nVerts, CGLTools.GLuint nTextureUnits):
        CGLTools.glBatchBegin(self._glBatch, primitive, nVerts, nTextureUnits)

    def copyVertexData3f(self, vVerts):
        # vVerts is actually a Python array so we need to copy it to a c array
        # then pass to glBatchCopyVertexData3f after that we free cFloats
        cdef float * cFloats
        cdef int i

        cFloats = <float *> PyMem_Malloc(len(vVerts)*cython.sizeof(float))
        if cFloats is NULL:
            raise MemoryError()
        for i in xrange(len(vVerts)):
            cFloats[i] = vVerts[i]
        CGLTools.glBatchCopyVertexData3f(self._glBatch, <CGLTools.M3DVector3f *>cFloats)
        PyMem_Free(cFloats)

    def vertex3f(self, float x, float y, float z):
        CGLTools.glBatchVertex3f(self._glBatch, x, y, z)

    def end(self):
        CGLTools.glBatchEnd(self._glBatch)

    def draw(self):
        CGLTools.glBatchDraw(self._glBatch)


cdef class GLTriangle:
    cdef CGLTools.GLTriangle *_glTriangle
    def __cinit__(self):
        self._glTriangle = <CGLTools.GLTriangle *>PyMem_Malloc(sizeof(CGLTools.GLTriangle))
        if not self._glTriangle:
            raise MemoryError()
        CGLTools.glTriangleBatchInit(self._glTriangle)

    def __dealloc__(self):
        if self._glTriangle is not NULL:
            CGLTools.glTriangleBatchDestroy(self._glTriangle)
            PyMem_Free(self._glTriangle)
            self._glTriangle = NULL

    def makeTorus(self, float majorRadius, float minorRadius, int numMajor, int numMinor):
        CGLTools.glToolsMakeTorus(self._glTriangle, majorRadius, minorRadius, numMajor, numMinor)


cdef class CStopWatch:
    cdef CGLTools.CStopWatch *_stopWatch
    def __cinit__(self):
        self._stopWatch = <CGLTools.CStopWatch *>PyMem_Malloc(sizeof(CGLTools.CStopWatch))
        if not self._stopWatch:
            raise MemoryError()
        CGLTools.stopWatchInit(self._stopWatch)

    def __dealloc__(self):
        if self._stopWatch is not NULL:
            PyMem_Free(self._stopWatch)
            self._stopWatch = NULL

    def reset(self):
        CGLTools.stopWatchReset(self._stopWatch)

    def getElapsedSeconds(self):
        return CGLTools.stopWatchGetElapsedSeconds(self._stopWatch)


cdef class GLMatrixStack:
    cdef CGLTools.GLMatrixStack *_glMatrixStack
    def __cinit__(self):
        self._glMatrixStack = <CGLTools.GLMatrixStack *>PyMem_Malloc(sizeof(CGLTools.GLMatrixStack))
        if not self._glMatrixStack:
            raise MemoryError()
        CGLTools.glMatrixStackInit(self._glMatrixStack, 64)

    def __dealloc__(self):
        if self._glMatrixStack is not NULL:
            PyMem_Free(self._glMatrixStack)
            self._glMatrixStack = NULL

    def loadMatrix(self, mMatrix):
        # mMatrix is actually a Python array so we need to copy it to a c array
        # then pass to glMatrixStackLoadMatrix after that we free cFloats
        cdef float * cFloats
        cdef int i

        cFloats = <float *> PyMem_Malloc(len(mMatrix)*cython.sizeof(float))
        if cFloats is NULL:
            raise MemoryError()
        for i in xrange(len(mMatrix)):
            cFloats[i] = mMatrix[i]
        CGLTools.glMatrixStackLoadMatrix(self._glMatrixStack, <CGLTools.M3DMatrix44f >cFloats)
        PyMem_Free(cFloats)

    def pushMatrix(self):
        CGLTools.glMatrixStackPushMatrix(self._glMatrixStack)

    def popMatrix(self):
        CGLTools.glMatrixStackPopMatrix(self._glMatrixStack)

    def translate(self, float x, float y, float z):
        CGLTools.glMatrixStackTranslate(self._glMatrixStack, x, y, z)

    def rotate(self, float angle, float x, float y, float z):
        CGLTools.glMatrixStackRotate(self._glMatrixStack, angle, x, y, z)

    cdef CGLTools.GLMatrixStack *getGLMatrixStack(self):
        return self._glMatrixStack


cdef class GLGeometryTransform:
    cdef CGLTools.GLGeometryTransform *_glGeometryTransform
    def __cinit__(self):
        self._glGeometryTransform = <CGLTools.GLGeometryTransform *>PyMem_Malloc(sizeof(CGLTools.GLGeometryTransform))
        if not self._glGeometryTransform:
            raise MemoryError()

    def __dealloc__(self):
        if self._glGeometryTransform is not NULL:
            PyMem_Free(self._glGeometryTransform)
            self._glGeometryTransform = NULL

    def setMatrixStacks(self, GLMatrixStack mModelView, GLMatrixStack mProjection):
        CGLTools.glGeometryTransformSetMatrixStacks(self._glGeometryTransform, mModelView.getGLMatrixStack(), 
                                                    mProjection.getGLMatrixStack())



cdef class GLTools:
    def __init__(self):
        pass

    def glewInit(self):
        err = CGLTools.glewInit()
        if err is not 0:
            raise Exception("Glew Error: %s" % (CGLTools.glewGetErrorString(err)))

    def setClearColor(self, float red, float green, float blue, float alpha):
        CGLTools.glToolsSetClearColor(red, green, blue, alpha)

    def clear(self):
        CGLTools.glToolsClear()

    def enableDepthTest(self):
        CGLTools.glToolsEnableDepthTest()

    def polygonMode(self):
        CGLTools.glToolsPolygonMode()


