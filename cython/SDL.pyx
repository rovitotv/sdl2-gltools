# This is wrapper class around SDL.  It imports CSDL.pxd for defitions to SDL.
# This class does all the error handling and takes care of the creation and
# destruction of SDL objects.
cimport CSDL

cdef class SDL:
    cdef CSDL.SDL_Window* _mainWindow
    cdef CSDL.SDL_GLContext _mainContext
    def __init__(self, windowName, int windowXSize, int windowYSize):
        if CSDL.SDL_Init(CSDL.SDL_INIT_VIDEO) is not 0:
            raise Exception(self.getError("can't init video"))
        if CSDL.SDL_GL_SetAttribute(CSDL.SDL_GL_CONTEXT_MAJOR_VERSION, 3) is not 0:
            raise Exception(self.getError("can't set GL_CONTEXT_MAJOR_VERSION"))
        if CSDL.SDL_GL_SetAttribute(CSDL.SDL_GL_CONTEXT_MINOR_VERSION, 2) is not 0:
            raise Exception(self.getError("can't set GL_CONTEXT_MINOR_VERSION"))
        if CSDL.SDL_GL_SetAttribute(CSDL.SDL_GL_DOUBLEBUFFER, 1) is not 0:
            raise Exception(self.getError("can't set GL_DOUBLEBUFFER"))
        if CSDL.SDL_GL_SetAttribute(CSDL.SDL_GL_DEPTH_SIZE, 24) is not 0:
            raise Exception(self.getError("can't set GL_DEPTH_SIZE"))
        self._mainWindow = CSDL.SDL_CreateWindow(windowName,
            CSDL.SDL_WINDOWPOS_CENTERED, CSDL.SDL_WINDOWPOS_CENTERED,
            windowXSize, windowYSize, CSDL.SDL_WINDOW_OPENGL or CSDL.SDL_WINDOW_SHOWN)
        if not self._mainWindow:
            raise Exception(self.getError("unable to create window"))
        self._mainContext = CSDL.SDL_GL_CreateContext(self._mainWindow)
        if not self._mainContext:
            raise Exception(self.getError("unable to get OpenGL Context"))

    def __dealloc__(self):
        CSDL.SDL_GL_DeleteContext(self._mainContext)
        CSDL.SDL_DestroyWindow(self._mainWindow)
        CSDL.SDL_Quit()

    cdef getError(self, error):
        errorString = ("error: %s SDL Error: %s" % (error, 
                                                    CSDL.SDL_GetError()))
        return errorString

    def delay(self, int mseconds):
        CSDL.SDL_Delay(mseconds)

    def GL_Swap_Window(self):
        CSDL.SDL_GL_SwapWindow(self._mainWindow)


