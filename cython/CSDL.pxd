# this is for SDL 2.0.1

cdef extern from "SDL.h":
    ctypedef unsigned char Uint8
    ctypedef unsigned long Uint32
    ctypedef unsigned long long Uint64
    ctypedef signed long long Sint64
    ctypedef unsigned short Uint16
    ctypedef void *SDL_GLContext
    cdef Uint32 SDL_INIT_VIDEO = 0x00000020
    cdef Uint32 SDL_WINDOWPOS_CENTERED = 0x2fff0000
    
    # structs and enums
    ctypedef struct SDL_Window:
        pass

    ctypedef enum SDL_WindowFlags:
        SDL_WINDOW_FULLSCREEN = 0x00000001
        SDL_WINDOW_OPENGL = 0x00000002
        SDL_WINDOW_SHOWN = 0x00000004
        SDL_WINDOW_HIDDEN = 0x00000008
        SDL_WINDOW_BORDERLESS = 0x00000010
        SDL_WINDOW_RESIZABLE = 0x00000020
        SDL_WINDOW_MINIMIZED = 0x00000040
        SDL_WINDOW_MAXIMIZED = 0x00000080
        SDL_WINDOW_INPUT_GRABBED = 0x00000100
        SDL_WINDOW_INPUT_FOCUS = 0x00000200
        SDL_WINDOW_MOUSE_FOCUS = 0x00000400
        SDL_WINDOW_FOREIGN = 0x00000800
        SDL_WINDOW_ALLOW_HIGHDPI = 0x00002000

    ctypedef enum SDL_GLattr:
        SDL_GL_RED_SIZE
        SDL_GL_GREEN_SIZE
        SDL_GL_BLUE_SIZE
        SDL_GL_ALPHA_SIZE
        SDL_GL_BUFFER_SIZE
        SDL_GL_DOUBLEBUFFER
        SDL_GL_DEPTH_SIZE
        SDL_GL_STENCIL_SIZE
        SDL_GL_ACCUM_RED_SIZE
        SDL_GL_ACCUM_GREEN_SIZE
        SDL_GL_ACCUM_BLUE_SIZE
        SDL_GL_ACCUM_ALPHA_SIZE
        SDL_GL_STEREO
        SDL_GL_MULTISAMPLEBUFFERS
        SDL_GL_MULTISAMPLESAMPLES
        SDL_GL_ACCELERATED_VISUAL
        SDL_GL_RETAINED_BACKING
        SDL_GL_CONTEXT_MAJOR_VERSION
        SDL_GL_CONTEXT_MINOR_VERSION
        SDL_GL_CONTEXT_EGL
        SDL_GL_CONTEXT_FLAGS
        SDL_GL_CONTEXT_PROFILE_MASK
        SDL_GL_SHARE_WITH_CURRENT_CONTEXT
        SDL_GL_FRAMEBUFFER_SRGB_CAPABLE

    # functions
    cdef const char* SDL_GetError()
    cdef int SDL_Init(Uint32 flags)
    cdef int SDL_GL_SetAttribute(SDL_GLattr attr, int value)
    cdef SDL_Window* SDL_CreateWindow(const char *title,
                             int         x,
                             int         y,
                             int         w,
                             int         h,
                             Uint32      flags)
    cdef SDL_GLContext SDL_GL_CreateContext(SDL_Window* window)
    cdef void SDL_GL_SwapWindow(SDL_Window* window)
    cdef void SDL_Delay(Uint32 ms)
    cdef void SDL_GL_DeleteContext(SDL_GLContext context)
    cdef void SDL_DestroyWindow(SDL_Window* window)
    cdef void SDL_Quit()
