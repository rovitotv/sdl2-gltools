# SphereWorld.py
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See LICENSE.txt for full details of the MPL license.
#
# Copyright Todd V. Rovito 2013
# https://bitbucket.org/rovitotv/sdl2-gltools
#
# This is a Python version of SphereWorld.c.  It use SDL2 and GLTools via
# Cython.  This example will draw a floor of green lines and a spinning red
# torus.
import SDL
import GLTools


def frange(start, end=None, inc=None):
    "A range function, that does accept float increments..."
    if end is None:
        end = start + 0.0
        start = 0.0
    if inc is None:
        inc = 1.0
    L = []
    while 1:
        next = start + len(L) * inc
        if inc > 0 and next >= end:
            break
        elif inc < 0 and next <= end:
            break
        L.append(next)
    return L


def SetupRC(shaderManager, glTools, floorBatch, toursBatch):
    glTools.enableDepthTest()
    glTools.polygonMode()
    glTools.setClearColor(0.0, 0.0, 0.0, 1.0)
    toursBatch.makeTorus(0.4, 0.15, 30, 30)
    floorBatch.begin(GLTools.GL_LINES, 324, 0)
    for x in frange(-20.0, 20.0, 0.5):
        floorBatch.vertex3f(x, -0.55, 20.0)
        floorBatch.vertex3f(x, -0.55, -20.0)
        floorBatch.vertex3f(20.0, -0.55, x)
        floorBatch.vertex3f(-20.0, -0.55, x)


def main():
    sdl = SDL.SDL(b"SphereWorld.py", 800, 600)
    glTools = GLTools.GLTools()
    glTools.glewInit()
    shaderManager = GLTools.GLShaderManager()  # must have OpenGL Context first
    floorBatch = GLTools.GLBatch()
    toursBatch = GLTools.GLTriangle()
    rotTimer = GLTools.CStopWatch()
    modelViewMatrix = GLTools.GLMatrixStack()
    projectionMatrix = GLTools.GLMatrixStack()

    SetupRC(shaderManager, glTools, floorBatch, toursBatch)

    #sdl.GL_Swap_Window()
    #sdl.delay(2000)


if __name__ == "__main__":
    main()
