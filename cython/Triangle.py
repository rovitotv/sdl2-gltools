# Triangle.py
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# See LICENSE.txt for full details of the MPL license.
#
# Copyright Todd V. Rovito 2013
# https://bitbucket.org/rovitotv/sdl2-gltools
#
# This is a Python version of Triangle.c.  It use SDL2 and GLTools via
# cython.  This example will draw a simple red triangle on the screen with
# a blue background.

# We'll use sys to properly exit with an error code.
#
# don't forget to set the path to the SDL library:
# export PYSDL2_DLL_PATH=/Volumes/SecureThreeB/SDL2/lib/
import SDL
import GLTools
import array


def SetupRC(triangleBatch, glTools):
    # blue background
    glTools.setClearColor(0.0, 0.0, 1.0, 1.0)

    # load up a triangle
    vVerts = array.array('f', [-0.5, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.5, 0.0])
    triangleBatch.begin(GLTools.GL_TRIANGLES, 3, 0)
    triangleBatch.copyVertexData3f(vVerts)
    triangleBatch.end()


def RenderScene(shaderManager, triangleBatch, glTools):
    glTools.clear()
    # make the red color
    vRed = array.array('f', [1.0, 0.0, 0.0, 1.0])
    shaderManager.useStockShader(GLTools.GLT_SHADER_IDENTITY, vRed)
    triangleBatch.draw()


def main():
    sdl = SDL.SDL(b"Triangle.py", 800, 600)
    glTools = GLTools.GLTools()
    glTools.glewInit()
    shaderManager = GLTools.GLShaderManager()  # must have OpenGL Context first
    triangleBatch = GLTools.GLBatch()
    SetupRC(triangleBatch, glTools)
    RenderScene(shaderManager, triangleBatch, glTools)
    sdl.GL_Swap_Window()
    sdl.delay(2000)


if __name__ == "__main__":
    main()
