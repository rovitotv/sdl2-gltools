# this is for SDL2-GLTools library to support Cython build
cdef extern from "GLTools.h":
    # OpenGL Types
    # lifted from GL.h
    # /opt/X11/include/GL
    ctypedef unsigned int  GLenum
    ctypedef unsigned char GLboolean
    ctypedef unsigned int  GLbitfield
    ctypedef void    GLvoid
    ctypedef signed char GLbyte        # 1-byte signed 
    ctypedef short   GLshort           # 2-byte signed 
    ctypedef int   GLint               # 4-byte signed 
    ctypedef unsigned char GLubyte     # 1-byte unsigned 
    ctypedef unsigned short  GLushort  # 2-byte unsigned 
    ctypedef unsigned int  GLuint      # 4-byte unsigned 
    ctypedef int   GLsizei             # 4-byte signed 
    ctypedef float   GLfloat           # single precision float 
    ctypedef float   GLclampf          # single precision float in [0,1] 
    ctypedef double    GLdouble        # double precision float 
    ctypedef double    GLclampd        # double precision float in [0,1] 

    # from math3D.h
    ctypedef float M3DVector2f[2]
    ctypedef double M3DVector2d[2]
    ctypedef float M3DVector3f[3]
    ctypedef double M3DVector3d[3]
    ctypedef float M3DVector4f[4]
    ctypedef double M3DVector4d[4]
    # 3x3 matrix - column major. X vector is 0, 1, 2, etc.
    #              0       3       6       
    #              1       4       7
    #              2       5       8
    ctypedef float   M3DMatrix33f[9]        # A 3 x 3 matrix, column major (floats) - OpenGL Style
    ctypedef double  M3DMatrix33d[9]        # A 3 x 3 matrix, column major (doubles) - OpenGL Style
    # 4x4 matrix - column major. X vector is 0, 1, 2, etc.
    #      0       4       8       12
    #      1       5       9       13
    #      2       6       10      14
    #      3       7       11      15
    ctypedef float M3DMatrix44f[16]         # A 4 X 4 matrix, column major (floats) - OpenGL style
    ctypedef double M3DMatrix44d[16]        # A 4 x 4 matrix, column major (doubles) - OpenGL style


    # from GLShaderManager.h ****************************************************
    ctypedef enum GLT_STOCK_SHADER:
        GLT_SHADER_IDENTITY = 0
        GLT_SHADER_FLAT
        GLT_SHADER_SHADED
        GLT_SHADER_DEFAULT_LIGHT
        GLT_SHADER_POINT_LIGHT_DIFF
        GLT_SHADER_TEXTURE_REPLACE
        GLT_SHADER_TEXTURE_MODULATE
        GLT_SHADER_TEXTURE_POINT_LIGHT_DIFF
        GLT_SHADER_TEXTURE_RECT_REPLACE
        GLT_SHADER_LAST

    ctypedef unsigned int uiStockShadersType[9]

    cdef int glShaderManagerInitializeStockShaders(GLuint *uiStockShaders)
    cdef void glShaderManagerDestruct(GLuint *uiStockShaders)
    cdef GLint glShaderManagerUseStockShader(GLuint *uiStockShaders, GLenum nShaderID, float *vColor) 

    # from GLBatch.h **************************************************************
    ctypedef struct GLBatchStruct:
        GLenum primitiveType
        GLuint uiVertexArray
        GLuint uiNormalArray
        GLuint uiColorArray
        GLuint *uiTextureCoordArray
        GLuint vertexArrayObject
    
        GLuint nVertsBuilding			# Building up vertexes counter (immediate mode emulator)
        GLuint nNumVerts		        # Number of verticies in this batch
        GLuint nNumTextureUnits 		# Number of texture coordinate sets
    
        int	bBatchDone			# Batch has been built
    
	
        M3DVector3f *pVerts
        M3DVector3f *pNormals
        M3DVector4f *pColors
        M3DVector2f **pTexCoords

    ctypedef GLBatchStruct GLBatch

    cdef void glBatchInitBatch(GLBatch *batch)
    cdef void glBatchBegin(GLBatch *batch, GLenum primitive, GLuint nVerts, GLuint nTextureUnits)
    cdef void glBatchCopyVertexData3f(GLBatch *batch, M3DVector3f *vVerts)
    cdef void glBatchEnd(GLBatch *batch)
    cdef void glBatchDraw(GLBatch *batch)
    cdef void glBatchDestruct(GLBatch *batch)
    cdef void glBatchVertex3f(GLBatch *batch, float x, float y, float z)

    # from GLTriangleBatch.h
    cdef int VERTEX_DATA = 0
    cdef int NORMAL_DATA = 1
    cdef int TEXTURE_DATA = 2
    cdef int INDEX_DATA = 3
    ctypedef struct GLTriangleStruct:
        GLushort  *pIndexes         # Array of indexes
        M3DVector3f *pVerts         # Array of vertices
        M3DVector3f *pNorms         # Array of normals
        M3DVector2f *pTexCoords     # Array of texture coordinates
        GLuint nMaxIndexes          # Maximum workspace
        GLuint nNumIndexes          # Number of indexes currently used
        GLuint nNumVerts            # Number of vertices actually used
        GLuint bufferObjects[4]
        GLuint vertexArrayBufferObject

    ctypedef GLTriangleStruct GLTriangle

    cdef void glTriangleBatchInit(GLTriangle *glTriangle)
    cdef void glTriangleBatchDestroy(GLTriangle *glTriangle)
    cdef void glTriangleBatchBeginMesh(GLTriangle *glTriangle, unsigned int nMaxVerts)
    cdef void glTriangleBatchAddTriangle(GLTriangle *glTriangle, M3DVector3f verts[3], 
                                         M3DVector3f vNorms[3], M3DVector2f vTexCoords[3])
    cdef void glTriangleBatchEnd(GLTriangle *glTriangle)
    cdef void glTriangleBatchDraw(GLTriangle *glTriangle)


    # from GLTools.h ***********************************************************
    cdef void glToolsSetClearColor(float red, float green, float blue, float alpha)
    cdef void glToolsClear()
    # from glew.h  GL/glew.h line 14384
    cdef GLenum glewInit()  # I placed it under GLTools because I was not sure where else it should go
    cdef const GLubyte* glewGetErrorString (GLenum error)
    cdef void glToolsMakeTorus(GLTriangle *torusBatch, float majorRadius, float minorRadius, 
                               int numMajor, int numMinor)
    cdef void glToolsEnableDepthTest()
    cdef void glToolsPolygonMode()


cdef extern from "StopWatch.h":
    ctypedef struct timeval:
        long int tv_sec
        long int tv_usec


    ctypedef struct CStopWatchStruct:
        timeval m_LastCount

    ctypedef CStopWatchStruct CStopWatch

    cdef void stopWatchInit(CStopWatch *sw)
    cdef void stopWatchReset(CStopWatch *sw)
    cdef float stopWatchGetElapsedSeconds(CStopWatch *sw)


cdef extern from "GLMatrixStack.h":
    ctypedef float M3DMatrix44f[16]         # A 4 X 4 matrix, column major (floats) - OpenGL style

    ctypedef enum GLT_STACK_ERROR:
         GLT_STACK_NOERROR = 0
         GLT_STACK_OVERFLOW
         GLT_STACK_UNDERFLOW

    ctypedef struct GLMatrixStackStruct:
        GLT_STACK_ERROR lastError
        int stackDepth
        int stackPointer
        M3DMatrix44f *pStack

    ctypedef GLMatrixStackStruct GLMatrixStack

    cdef void glMatrixStackInit(GLMatrixStack *matrixStack, int iStackDepth)
    cdef void glMatrixStackDestroy(GLMatrixStack *matrixStack)
    cdef void glMatrixStackLoadIdentity(GLMatrixStack *matrixStack)
    cdef void glMatrixStackLoadMatrix(GLMatrixStack *matrixStack, const M3DMatrix44f mMatrix)
    # need a GLFrame struct to use this function
    #cdef void glMatrixStackLoadMatrixFromFrame(GLMatrixStack *matrixStack, GLFrame *frame)
    cdef void glMatrixStackMultiplyMatrix(GLMatrixStack *matrixStack, const M3DMatrix44f mMatrix)
    #cdef void glMatrixStackMultiplyMatrixFromFrame(GLMatrixStack *matrixStack, GLFrame *frame)
    cdef void glMatrixStackPushMatrix(GLMatrixStack *matrixStack)
    cdef void glMatrixStackPopMatrix(GLMatrixStack *matrixStack)
    cdef void glMatrixStackScale(GLMatrixStack *matrixStack, float x, float y, float z)
    cdef void glMatrixStackTranslate(GLMatrixStack *matrixStack, float x, float y, float z)
    cdef void glMatrixStackRotate(GLMatrixStack *matrixStack, float angle, float x, float y, float z)
    cdef void glMatrixStackScaleVector(GLMatrixStack *matrixStack, const M3DVector3f vScale)
    cdef void glMatrixStackTranslateVector(GLMatrixStack *matrixStack, const M3DVector3f vTranslate)
    cdef void glMatrixStackRotateVector(GLMatrixStack *matrixStack, float angle, M3DVector3f vAxis)
    cdef void glMatrixStackPushNewMatrix(GLMatrixStack *matrixStack, const M3DMatrix44f mMatrix)
    #cdef void glMatrixStackPushNewMatrixFromFrame(GLMatrixStack *matrixStack, GLFrame *frame)
    cdef M3DMatrix44f *glMatrixStackGetMatrix(GLMatrixStack *matrixStack)
    cdef void glMatrixStackGetMatrixCopy(GLMatrixStack *matrixStack, M3DMatrix44f mMatrix)
    cdef GLT_STACK_ERROR glMatrixStackGetLastError(GLMatrixStack *matrixStack)


cdef extern from "GLGeometryTransform.h":

    ctypedef float M3DMatrix44f[16]
    ctypedef float M3DMatrix33f[9]
    ctypedef enum GLT_STACK_ERROR:
         GLT_STACK_NOERROR = 0
         GLT_STACK_OVERFLOW
         GLT_STACK_UNDERFLOW

    ctypedef struct GLMatrixStackStruct:
        GLT_STACK_ERROR lastError
        int stackDepth
        int stackPointer
        M3DMatrix44f *pStack

    ctypedef GLMatrixStackStruct GLMatrixStack
    
    ctypedef struct GLGeometryTransformStruct:
        M3DMatrix44f    _mModelViewProjection
        M3DMatrix33f    _mNormalMatrix
        GLMatrixStack   *_mModelView
        GLMatrixStack   *_mProjection

    ctypedef GLGeometryTransformStruct GLGeometryTransform
    ctypedef int bool

    cdef void glGeometryTransformSetModelViewMatrixStack(GLGeometryTransform *geoTransform, GLMatrixStack *mModelView)
    cdef void glGeometryTransformSetProjectionMatrixStack(GLGeometryTransform *geoTransform, GLMatrixStack *mProjection)
    cdef void glGeometryTransformSetMatrixStacks(GLGeometryTransform *geoTransform, GLMatrixStack *mModelView, GLMatrixStack *mProjection)
    cdef const M3DMatrix44f *glGeometryTransformGetModelViewProjectionMatrix(GLGeometryTransform *geoTransform)
    cdef const M3DMatrix44f *glGeometryTransformGetModelViewMatrix(GLGeometryTransform *geoTransform)
    cdef const M3DMatrix44f *glGeometryTransformGetProjectionMatrix(GLGeometryTransform *geoTransform)
    cdef const M3DMatrix33f *glGeometryTransformGetNormalMatrix(GLGeometryTransform *geoTransform, bool bNormalize)


