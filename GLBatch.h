/*
GLBatch.h

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright (c) 2013, Todd V. Rovito
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef GLBatch_h
#define GLBatch_h

// Bring in OpenGL
// Windows
#ifdef WIN32
#include <windows.h>		// Must have for Windows platform builds
#ifndef GLEW_STATIC
#define GLEW_STATIC
#endif
#include <gl/glew.h>			// OpenGL Extension "autoloader"
#include <gl/gl.h>			// Microsoft OpenGL headers (version 1.1 by themselves)
#endif

// Mac OS X
#ifdef __APPLE__
#include <TargetConditionals.h>
#if TARGET_OS_IPHONE | TARGET_IPHONE_SIMULATOR
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#define OPENGL_ES
#else
#include <GL/glew.h>
#include <OpenGL/gl.h>		// Apple OpenGL haders (version depends on OS X SDK version)
#endif
#endif

// Linux
#ifdef linux
#define GLEW_STATIC
#include <glew.h>
#endif

#include "GLShaderManager.h"
#include "math3d.h"

struct GLBatchStruct {
    GLenum primitiveType;
    GLuint uiVertexArray;
    GLuint uiNormalArray;
    GLuint uiColorArray;
    GLuint *uiTextureCoordArray;
    GLuint vertexArrayObject;
    
    GLuint nVertsBuilding;			// Building up vertexes counter (immediate mode emulator)
    GLuint nNumVerts;				// Number of verticies in this batch
    GLuint nNumTextureUnits;		// Number of texture coordinate sets
    
    int	bBatchDone;				// Batch has been built
    
	
    M3DVector3f *pVerts;
    M3DVector3f *pNormals;
    M3DVector4f *pColors;
    M3DVector2f **pTexCoords;

};

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtypedef-redefinition"
typedef struct GLBatchStruct GLBatch;
#pragma clang diagnostic pop

void glBatchInitBatch(GLBatch *batch);
void glBatchBegin(GLBatch *batch, GLenum primitive, GLuint nVerts, GLuint nTextureUnits);
void glBatchCopyVertexData3f(GLBatch *batch, M3DVector3f *vVerts);
void glBatchEnd(GLBatch *batch);
void glBatchDraw(GLBatch *batch);
void glBatchVertex3f(GLBatch *batch, float x, float y, float z);
void glBatchNormal3f(GLBatch *batch, GLfloat x, GLfloat y, GLfloat z);
void glBatchMultiTexCoord2f(GLBatch *batch, GLuint texture, GLclampf s, GLclampf t);
void glBatchDestruct(GLBatch *batch);



#endif
