/*
GLTools.c

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright Todd V. Rovito 2013
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include "GLTools.h"

//////////////////////////////////////////////////////////////////////////
// Load the shader from the source text
void glToolsLoadShaderSrc(const char *szShaderSrc, GLuint shader)
{
    GLchar *fsStringPtr[1];
    
    fsStringPtr[0] = (GLchar *)szShaderSrc;
    glShaderSource(shader, 1, (const GLchar **)fsStringPtr, NULL);
}


/////////////////////////////////////////////////////////////////
// Load a pair of shaders, compile, and link together. Specify the complete
// source code text for each shader. Note, there is no support for
// just loading say a vertex program... you have to do both.
GLuint glToolsLoadShaderPairSrcWithAttributes(const char *szVertexSrc, const char *szFragmentSrc, ...)
{
    // Temporary Shader objects
    GLuint hVertexShader;
    GLuint hFragmentShader;
    GLuint hReturn = 0;
    GLint testVal;
	
    // Create shader objects
    hVertexShader = glCreateShader(GL_VERTEX_SHADER);
    hFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	
    // Load them.
    glToolsLoadShaderSrc(szVertexSrc, hVertexShader);
    glToolsLoadShaderSrc(szFragmentSrc, hFragmentShader);
    
    // Compile them
    glCompileShader(hVertexShader);
    glCompileShader(hFragmentShader);
    
    // Check for errors
    glGetShaderiv(hVertexShader, GL_COMPILE_STATUS, &testVal);
    if(testVal == GL_FALSE)
    {
        glDeleteShader(hVertexShader);
        glDeleteShader(hFragmentShader);
        return (GLuint)NULL;
    }
    
    glGetShaderiv(hFragmentShader, GL_COMPILE_STATUS, &testVal);
    if(testVal == GL_FALSE)
    {
        glDeleteShader(hVertexShader);
        glDeleteShader(hFragmentShader);
        return (GLuint)NULL;
    }
    
    // Link them - assuming it works...
    hReturn = glCreateProgram();
    glAttachShader(hReturn, hVertexShader);
    glAttachShader(hReturn, hFragmentShader);
    
	// List of attributes
	va_list attributeList;
	va_start(attributeList, szFragmentSrc);
    
	char *szNextArg;
	int iArgCount = va_arg(attributeList, int);	// Number of attributes
	for(int i = 0; i < iArgCount; i++)
    {
		int index = va_arg(attributeList, int);
		szNextArg = va_arg(attributeList, char*);
		glBindAttribLocation(hReturn, index, szNextArg);
    }
	va_end(attributeList);
    
    
    glLinkProgram(hReturn);
	
    // These are no longer needed
    glDeleteShader(hVertexShader);
    glDeleteShader(hFragmentShader);
    
    // Make sure link worked too
    glGetProgramiv(hReturn, GL_LINK_STATUS, &testVal);
    if(testVal == GL_FALSE)
    {
		glDeleteProgram(hReturn);
		return (GLuint)NULL;
    }
    
    return hReturn;  
}

void glToolsSetClearColor(float red, float green, float blue, float alpha) {
    // specify clear values for the color buffers
    glClearColor(red, green, blue, alpha);
}

void glToolsClear() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void glToolsMakeTorus(GLTriangle *torusBatch, float majorRadius, float minorRadius, int numMajor, int numMinor) {
    double majorStep = 2.0f*M3D_PI / numMajor;
    double minorStep = 2.0f*M3D_PI / numMinor;
    int i, j;
	
    glTriangleBatchBeginMesh(torusBatch, numMajor * (numMinor+1) * 6);
    for (i=0; i<numMajor; ++i)
    {
		double a0 = i * majorStep;
		double a1 = a0 + majorStep;
		GLfloat x0 = (GLfloat) cos(a0);
		GLfloat y0 = (GLfloat) sin(a0);
		GLfloat x1 = (GLfloat) cos(a1);
		GLfloat y1 = (GLfloat) sin(a1);
        
		M3DVector3f vVertex[4];
		M3DVector3f vNormal[4];
		M3DVector2f vTexture[4];
		
		for (j=0; j<=numMinor; ++j)
        {
			double b = j * minorStep;
			GLfloat c = (GLfloat) cos(b);
			GLfloat r = minorRadius * c + majorRadius;
			GLfloat z = minorRadius * (GLfloat) sin(b);
			
			// First point
			vTexture[0][0] = (float)(i)/(float)(numMajor);
			vTexture[0][1] = (float)(j)/(float)(numMinor);
			vNormal[0][0] = x0*c;
			vNormal[0][1] = y0*c;
			vNormal[0][2] = z/minorRadius;
			m3dNormalizeVector3f(vNormal[0]);
			vVertex[0][0] = x0 * r;
			vVertex[0][1] = y0 * r;
			vVertex[0][2] = z;
			
			// Second point
			vTexture[1][0] = (float)(i+1)/(float)(numMajor);
			vTexture[1][1] = (float)(j)/(float)(numMinor);
			vNormal[1][0] = x1*c;
			vNormal[1][1] = y1*c;
			vNormal[1][2] = z/minorRadius;
			m3dNormalizeVector3f(vNormal[1]);
			vVertex[1][0] = x1*r;
			vVertex[1][1] = y1*r;
			vVertex[1][2] = z;
            
			// Next one over
			b = (j+1) * minorStep;
			c = (GLfloat) cos(b);
			r = minorRadius * c + majorRadius;
			z = minorRadius * (GLfloat) sin(b);
            
			// Third (based on first)
			vTexture[2][0] = (float)(i)/(float)(numMajor);
			vTexture[2][1] = (float)(j+1)/(float)(numMinor);
			vNormal[2][0] = x0*c;
			vNormal[2][1] = y0*c;
			vNormal[2][2] = z/minorRadius;
			m3dNormalizeVector3f(vNormal[2]);
			vVertex[2][0] = x0 * r;
			vVertex[2][1] = y0 * r;
			vVertex[2][2] = z;
			
			// Fourth (based on second)
			vTexture[3][0] = (float)(i+1)/(float)(numMajor);
			vTexture[3][1] = (float)(j+1)/(float)(numMinor);
			vNormal[3][0] = x1*c;
			vNormal[3][1] = y1*c;
			vNormal[3][2] = z/minorRadius;
			m3dNormalizeVector3f(vNormal[3]);
			vVertex[3][0] = x1*r;
			vVertex[3][1] = y1*r;
			vVertex[3][2] = z;
            
            glTriangleBatchAddTriangle(torusBatch, vVertex, vNormal, vTexture);
			
			// Rearrange for next triangle
			memcpy(vVertex[0], vVertex[1], sizeof(M3DVector3f));
			memcpy(vNormal[0], vNormal[1], sizeof(M3DVector3f));
			memcpy(vTexture[0], vTexture[1], sizeof(M3DVector2f));
			
			memcpy(vVertex[1], vVertex[3], sizeof(M3DVector3f));
			memcpy(vNormal[1], vNormal[3], sizeof(M3DVector3f));
			memcpy(vTexture[1], vTexture[3], sizeof(M3DVector2f));
            
            glTriangleBatchAddTriangle(torusBatch, vVertex, vNormal, vTexture);
        }
    }
    glTriangleBatchEnd(torusBatch);
}

void glToolsMakeSphere(GLTriangle *sphereBatch, GLfloat fRadius, GLint iSlices, GLint iStacks) {
    GLfloat drho = (GLfloat)(3.141592653589) / (GLfloat) iStacks;
    GLfloat dtheta = 2.0f * (GLfloat)(3.141592653589) / (GLfloat) iSlices;
    GLfloat ds = 1.0f / (GLfloat) iSlices;
    GLfloat dt = 1.0f / (GLfloat) iStacks;
    GLfloat t = 1.0f;
    GLfloat s = 0.0f;
    GLint i, j;     // Looping variables
    
    glTriangleBatchBeginMesh(sphereBatch, iSlices * iStacks * 6);
    for (i = 0; i < iStacks; i++)
    {
        GLfloat rho = (GLfloat)i * drho;
        GLfloat srho = (GLfloat)(sin(rho));
        GLfloat crho = (GLfloat)(cos(rho));
        GLfloat srhodrho = (GLfloat)(sin(rho + drho));
        GLfloat crhodrho = (GLfloat)(cos(rho + drho));
        
        // Many sources of OpenGL sphere drawing code uses a triangle fan
        // for the caps of the sphere. This however introduces texturing
        // artifacts at the poles on some OpenGL implementations
        s = 0.0f;
        M3DVector3f vVertex[4];
        M3DVector3f vNormal[4];
        M3DVector2f vTexture[4];
        
        for ( j = 0; j < iSlices; j++)
        {
            GLfloat theta = (j == iSlices) ? 0.0f : j * dtheta;
            GLfloat stheta = (GLfloat)(-sin(theta));
            GLfloat ctheta = (GLfloat)(cos(theta));
            
            GLfloat x = stheta * srho;
            GLfloat y = ctheta * srho;
            GLfloat z = crho;
            
            vTexture[0][0] = s;
            vTexture[0][1] = t;
            vNormal[0][0] = x;
            vNormal[0][1] = y;
            vNormal[0][2] = z;
            vVertex[0][0] = x * fRadius;
            vVertex[0][1] = y * fRadius;
            vVertex[0][2] = z * fRadius;
            
            x = stheta * srhodrho;
            y = ctheta * srhodrho;
            z = crhodrho;
            
            vTexture[1][0] = s;
            vTexture[1][1] = t - dt;
            vNormal[1][0] = x;
            vNormal[1][1] = y;
            vNormal[1][2] = z;
            vVertex[1][0] = x * fRadius;
            vVertex[1][1] = y * fRadius;
            vVertex[1][2] = z * fRadius;
            
            
            theta = ((j+1) == iSlices) ? 0.0f : (j+1) * dtheta;
            stheta = (GLfloat)(-sin(theta));
            ctheta = (GLfloat)(cos(theta));
            
            x = stheta * srho;
            y = ctheta * srho;
            z = crho;
            
            s += ds;
            vTexture[2][0] = s;
            vTexture[2][1] = t;
            vNormal[2][0] = x;
            vNormal[2][1] = y;
            vNormal[2][2] = z;
            vVertex[2][0] = x * fRadius;
            vVertex[2][1] = y * fRadius;
            vVertex[2][2] = z * fRadius;
            
            x = stheta * srhodrho;
            y = ctheta * srhodrho;
            z = crhodrho;
            
            vTexture[3][0] = s;
            vTexture[3][1] = t - dt;
            vNormal[3][0] = x;
            vNormal[3][1] = y;
            vNormal[3][2] = z;
            vVertex[3][0] = x * fRadius;
            vVertex[3][1] = y * fRadius;
            vVertex[3][2] = z * fRadius;
            
            glTriangleBatchAddTriangle(sphereBatch, vVertex, vNormal, vTexture);
			
            
            // Rearrange for next triangle
            memcpy(vVertex[0], vVertex[1], sizeof(M3DVector3f));
            memcpy(vNormal[0], vNormal[1], sizeof(M3DVector3f));
            memcpy(vTexture[0], vTexture[1], sizeof(M3DVector2f));
            
            memcpy(vVertex[1], vVertex[3], sizeof(M3DVector3f));
            memcpy(vNormal[1], vNormal[3], sizeof(M3DVector3f));
            memcpy(vTexture[1], vTexture[3], sizeof(M3DVector2f));
            
            glTriangleBatchAddTriangle(sphereBatch, vVertex, vNormal, vTexture);
        }
        t -= dt;
    }
    glTriangleBatchEnd(sphereBatch);
}

///////////////////////////////////////////////////////////////////////////////////////
// Make a cube, centered at the origin, and with a specified "radius"
void glToolsMakeCube(GLBatch *cubeBatch, GLfloat fRadius )
{
    glBatchBegin(cubeBatch, GL_TRIANGLES, 36, 1);
    
    /////////////////////////////////////////////
    // Top of cube
    glBatchNormal3f(cubeBatch, 0.0f, fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, fRadius);
    glBatchVertex3f(cubeBatch, fRadius, fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, 0.0f);
    glBatchVertex3f(cubeBatch, fRadius, fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, 0.0f);
    glBatchVertex3f(cubeBatch, -fRadius, fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, fRadius);
    glBatchVertex3f(cubeBatch, fRadius, fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, 0.0f);
    glBatchVertex3f(cubeBatch, -fRadius, fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, fRadius);
    glBatchVertex3f(cubeBatch, -fRadius, fRadius, fRadius);
    
    
    ////////////////////////////////////////////
    // Bottom of cube
    glBatchNormal3f(cubeBatch, 0.0f, -fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, 0.0f);
    glBatchVertex3f(cubeBatch, -fRadius, -fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, -fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, 0.0f);
    glBatchVertex3f(cubeBatch, fRadius, -fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, -fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, fRadius);
    glBatchVertex3f(cubeBatch, fRadius, -fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, -fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, fRadius);
    glBatchVertex3f(cubeBatch, -fRadius, -fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, -fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, 0.0f);
    glBatchVertex3f(cubeBatch, -fRadius, -fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, -fRadius, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, fRadius);
    glBatchVertex3f(cubeBatch, fRadius, -fRadius, fRadius);
    
    ///////////////////////////////////////////
    // Left side of cube
    glBatchNormal3f(cubeBatch, -fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, fRadius);
    glBatchVertex3f(cubeBatch, -fRadius, fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, -fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, 0.0f);
    glBatchVertex3f(cubeBatch, -fRadius, fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, -fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, 0.0f);
    glBatchVertex3f(cubeBatch, -fRadius, -fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, -fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, fRadius);
    glBatchVertex3f(cubeBatch, -fRadius, fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, -fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, 0.0f);
    glBatchVertex3f(cubeBatch, -fRadius, -fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, -fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, fRadius);
    glBatchVertex3f(cubeBatch, -fRadius, -fRadius, fRadius);
    
    // Right side of cube
    glBatchNormal3f(cubeBatch, fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, 0.0f);
    glBatchVertex3f(cubeBatch, fRadius, -fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, 0.0f);
    glBatchVertex3f(cubeBatch, fRadius, fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, fRadius);
    glBatchVertex3f(cubeBatch, fRadius, fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, fRadius);
    glBatchVertex3f(cubeBatch, fRadius, fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, fRadius);
    glBatchVertex3f(cubeBatch, fRadius, -fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, fRadius, 0.0f, 0.0f);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, 0.0f);
    glBatchVertex3f(cubeBatch, fRadius, -fRadius, -fRadius);
    
    // Front and Back
    // Front
    glBatchNormal3f(cubeBatch, 0.0f, 0.0f, fRadius);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, 0.0f);
    glBatchVertex3f(cubeBatch, fRadius, -fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, 0.0f, fRadius);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, fRadius);
    glBatchVertex3f(cubeBatch, fRadius, fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, 0.0f, fRadius);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, fRadius);
    glBatchVertex3f(cubeBatch, -fRadius, fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, 0.0f, fRadius);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, fRadius);
    glBatchVertex3f(cubeBatch, -fRadius, fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, 0.0f, fRadius);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, 0.0f);
    glBatchVertex3f(cubeBatch, -fRadius, -fRadius, fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, 0.0f, fRadius);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, 0.0f);
    glBatchVertex3f(cubeBatch, fRadius, -fRadius, fRadius);
    
    // Back
    glBatchNormal3f(cubeBatch, 0.0f, 0.0f, -fRadius);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, 0.0f);
    glBatchVertex3f(cubeBatch, fRadius, -fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, 0.0f, -fRadius);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, 0.0f);
    glBatchVertex3f(cubeBatch, -fRadius, -fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, 0.0f, -fRadius);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, fRadius);
    glBatchVertex3f(cubeBatch, -fRadius, fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, 0.0f, -fRadius);
    glBatchMultiTexCoord2f(cubeBatch, 0, 0.0f, fRadius);
    glBatchVertex3f(cubeBatch, -fRadius, fRadius, -fRadius);
    
    glBatchNormal3f(cubeBatch, 0.0f, 0.0f, -fRadius);
    glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, fRadius);
    glBatchVertex3f(cubeBatch, fRadius, fRadius, -fRadius);
    
	glBatchNormal3f(cubeBatch, 0.0f, 0.0f, -fRadius);
	glBatchMultiTexCoord2f(cubeBatch, 0, fRadius, 0.0f);
	glBatchVertex3f(cubeBatch, fRadius, -fRadius, -fRadius);
    glBatchEnd(cubeBatch);
}

void glToolsEnableDepthTest() {
    glEnable(GL_DEPTH_TEST);
}

void glToolsPolygonMode() {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void glToolsClearColorDepthBuffer() {
	// Clear the color and depth buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

