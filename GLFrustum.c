/*
GLFrustum.c

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright Todd V. Rovito 2013
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include "GLFrustum.h"

void glFrustumInitDefault(GLFrustum *frustum) {
    glFrustumSetOrthographic(frustum, -1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);
}

void glFrustumInitPerspective(GLFrustum *frustum, float fFov, float fAspect, float fNear, float fFar) {
    glFrustumSetPerspective(frustum, fFov, fAspect, fNear, fFar);
}

void glFrustumInitOrthographic(GLFrustum *frustum, float xMin, float xMax, float yMin, float yMax, float zMin, float zMax) {
    glFrustumSetOrthographic(frustum, xMin, xMax, yMin, yMax, zMin, zMax);
}

void glFrustumSetOrthographic(GLFrustum *frustum, float xMin, float xMax, float yMin, float yMax, float zMin, float zMax) {
    m3dMakeOrthographicMatrix(frustum->projMatrix, xMin, xMax, yMin, yMax, zMin, zMax);
    frustum->projMatrix[15] = 1.0f;
    
    
    // Fill in values for untransformed Frustum corners
    // Near Upper Left
    frustum->nearUL[0] = xMin; frustum->nearUL[1] = yMax; frustum->nearUL[2] = zMin; frustum->nearUL[3] = 1.0f;
    
    // Near Lower Left
    frustum->nearLL[0] = xMin; frustum->nearLL[1] = yMin; frustum->nearLL[2] = zMin; frustum->nearLL[3] = 1.0f;
    
    // Near Upper Right
    frustum->nearUR[0] = xMax; frustum->nearUR[1] = yMax; frustum->nearUR[2] = zMin; frustum->nearUR[3] = 1.0f;
    
    // Near Lower Right
    frustum->nearLR[0] = xMax; frustum->nearLR[1] = yMin; frustum->nearLR[2] = zMin; frustum->nearLR[3] = 1.0f;
    
    // Far Upper Left
    frustum->farUL[0] = xMin; frustum->farUL[1] = yMax; frustum->farUL[2] = zMax; frustum->farUL[3] = 1.0f;
    
    // Far Lower Left
    frustum->farLL[0] = xMin; frustum->farLL[1] = yMin; frustum->farLL[2] = zMax; frustum->farLL[3] = 1.0f;
    
    // Far Upper Right
    frustum->farUR[0] = xMax; frustum->farUR[1] = yMax; frustum->farUR[2] = zMax; frustum->farUR[3] = 1.0f;
    
    // Far Lower Right
    frustum->farLR[0] = xMax; frustum->farLR[1] = yMin; frustum->farLR[2] = zMax; frustum->farLR[3] = 1.0f;
    
}

void glFrustumSetPerspective(GLFrustum *frustum, float fFov, float fAspect, float fNear, float fFar) {
    float xmin, xmax, ymin, ymax;       // Dimensions of near clipping plane
    float xFmin, xFmax, yFmin, yFmax;   // Dimensions of far clipping plane
    
    // Do the Math for the near clipping plane
    ymax = fNear * tanf( fFov * M3D_PI / 360.0 );
    ymin = -ymax;
    xmin = ymin * fAspect;
    xmax = -xmin;
    
    // Construct the projection matrix
    m3dLoadIdentity44f(frustum->projMatrix);
    frustum->projMatrix[0] = (2.0f * fNear)/(xmax - xmin);
    frustum->projMatrix[5] = (2.0f * fNear)/(ymax - ymin);
    frustum->projMatrix[8] = (xmax + xmin) / (xmax - xmin);
    frustum->projMatrix[9] = (ymax + ymin) / (ymax - ymin);
    frustum->projMatrix[10] = -((fFar + fNear)/(fFar - fNear));
    frustum->projMatrix[11] = -1.0f;
    frustum->projMatrix[14] = -((2.0f * fFar * fNear)/(fFar - fNear));
    frustum->projMatrix[15] = 0.0f;
    
    // Do the Math for the far clipping plane
    yFmax = fFar * tanf(fFov * M3D_PI / 360.0);
    yFmin = -yFmax;
    xFmin = yFmin * fAspect;
    xFmax = -xFmin;
    
    
    // Fill in values for untransformed Frustum corners
    // Near Upper Left
    frustum->nearUL[0] = xmin; frustum->nearUL[1] = ymax; frustum->nearUL[2] = -fNear; frustum->nearUL[3] = 1.0f;
    
    // Near Lower Left
    frustum->nearLL[0] = xmin; frustum->nearLL[1] = ymin; frustum->nearLL[2] = -fNear; frustum->nearLL[3] = 1.0f;
    
    // Near Upper Right
    frustum->nearUR[0] = xmax; frustum->nearUR[1] = ymax; frustum->nearUR[2] = -fNear; frustum->nearUR[3] = 1.0f;
    
    // Near Lower Right
    frustum->nearLR[0] = xmax; frustum->nearLR[1] = ymin; frustum->nearLR[2] = -fNear; frustum->nearLR[3] = 1.0f;
    
    // Far Upper Left
    frustum->farUL[0] = xFmin; frustum->farUL[1] = yFmax; frustum->farUL[2] = -fFar; frustum->farUL[3] = 1.0f;
    
    // Far Lower Left
    frustum->farLL[0] = xFmin; frustum->farLL[1] = yFmin; frustum->farLL[2] = -fFar; frustum->farLL[3] = 1.0f;
    
    // Far Upper Right
    frustum->farUR[0] = xFmax; frustum->farUR[1] = yFmax; frustum->farUR[2] = -fFar; frustum->farUR[3] = 1.0f;
    
    // Far Lower Right
    frustum->farLR[0] = xFmax; frustum->farLR[1] = yFmin; frustum->farLR[2] = -fFar; frustum->farLR[3] = 1.0f;
}

void glFrustumTransform(GLFrustum *frustum, GLFrame *camera) {
    // Workspace
    M3DMatrix44f rotMat;
    M3DVector3f vForward, vUp, vCross;
    M3DVector3f   vOrigin;
    
    ///////////////////////////////////////////////////////////////////
    // Create the transformation matrix. This was the trickiest part
    // for me. The default view from OpenGL is down the negative Z
    // axis. However, building a transformation axis from these
    // directional vectors points the frustum the wrong direction. So
    // You must reverse them here, or build the initial frustum
    // backwards - which to do is purely a matter of taste. I chose to
    // compensate here to allow better operability with some of my other
    // legacy code and projects. RSW
    glFrameGetForwardVector(camera, vForward);
    vForward[0] = -vForward[0];
    vForward[1] = -vForward[1];
    vForward[2] = -vForward[2];
    
    glFrameGetUpVector(camera, vUp);
    glFrameGetOrigin(camera, vOrigin);
    
    // Calculate the right side (x) vector
    m3dCrossProduct3f(vCross, vUp, vForward);
    
    // The Matrix
    // X Column
    memcpy(rotMat, vCross, sizeof(float)*3);
    rotMat[3] = 0.0f;
    
    // Y Column
    memcpy(&rotMat[4], vUp, sizeof(float)*3);
    rotMat[7] = 0.0f;
    
    // Z Column
    memcpy(&rotMat[8], vForward, sizeof(float)*3);
    rotMat[11] = 0.0f;
    
    // Translation
    rotMat[12] = vOrigin[0];
    rotMat[13] = vOrigin[1];
    rotMat[14] = vOrigin[2];
    rotMat[15] = 1.0f;
    
    ////////////////////////////////////////////////////
    // Transform the frustum corners
    m3dTransformVector4f(frustum->nearULT, frustum->nearUL, rotMat);
    m3dTransformVector4f(frustum->nearLLT, frustum->nearLL, rotMat);
    m3dTransformVector4f(frustum->nearURT, frustum->nearUR, rotMat);
    m3dTransformVector4f(frustum->nearLRT, frustum->nearLR, rotMat);
    m3dTransformVector4f(frustum->farULT, frustum->farUL, rotMat);
    m3dTransformVector4f(frustum->farLLT, frustum->farLL, rotMat);
    m3dTransformVector4f(frustum->farURT, frustum->farUR, rotMat);
    m3dTransformVector4f(frustum->farLRT, frustum->farLR, rotMat);
    
    ////////////////////////////////////////////////////
    // Derive Plane Equations from points... Points given in
    // counter clockwise order to make normals point inside
    // the Frustum
    // Near and Far Planes
    m3dGetPlaneEquationf(frustum->nearPlane, frustum->nearULT, frustum->nearLLT, frustum->nearLRT);
    m3dGetPlaneEquationf(frustum->farPlane, frustum->farULT, frustum->farURT, frustum->farLRT);
    
    // Top and Bottom Planes
    m3dGetPlaneEquationf(frustum->topPlane, frustum->nearULT, frustum->nearURT, frustum->farURT);
    m3dGetPlaneEquationf(frustum->bottomPlane, frustum->nearLLT, frustum->farLLT, frustum->farLRT);
    
    // Left and right planes
    m3dGetPlaneEquationf(frustum->leftPlane, frustum->nearLLT, frustum->nearULT, frustum->farULT);
    m3dGetPlaneEquationf(frustum->rightPlane, frustum->nearLRT, frustum->farLRT, frustum->farURT);
}

bool glFrustumTestSphere(GLFrustum *frustum, M3DVector3f vPoint, float fRadius) {
    float fDist;
    
    // Near Plane - See if it is behind me
    fDist = m3dGetDistanceToPlanef(vPoint, frustum->nearPlane);
    if(fDist + fRadius <= 0.0)
        return false;
    
    // Distance to far plane
    fDist = m3dGetDistanceToPlanef(vPoint, frustum->farPlane);
    if(fDist + fRadius <= 0.0)
        return false;
    
    fDist = m3dGetDistanceToPlanef(vPoint, frustum->leftPlane);
    if(fDist + fRadius <= 0.0)
        return false;
    
    fDist = m3dGetDistanceToPlanef(vPoint, frustum->rightPlane);
    if(fDist + fRadius <= 0.0)
        return false;
    
    fDist = m3dGetDistanceToPlanef(vPoint, frustum->bottomPlane);
    if(fDist + fRadius <= 0.0)
        return false;
    
    fDist = m3dGetDistanceToPlanef(vPoint, frustum->topPlane);
    if(fDist + fRadius <= 0.0)
        return false;
    
    return true;
}

bool glFrustumTestSpherePoint(GLFrustum *frustum, float x, float y, float z, float fRadius) {
    M3DVector3f vPoint;
    vPoint[0] = x;
    vPoint[1] = y;
    vPoint[2] = z;
    
    return glFrustumTestSphere(frustum, vPoint, fRadius);
}

const M3DMatrix44f *glFrustumGetProjectionMatrix(GLFrustum *frustum) {
    return &frustum->projMatrix;
}





