/*
GLGeometryTransform.c

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright Todd V. Rovito 2013
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include "GLGeometryTransform.h"

void glGeometryTransformSetModelViewMatrixStack(GLGeometryTransform *geoTransform, GLMatrixStack *mModelView) {
    geoTransform->_mModelView = mModelView;
}

void glGeometryTransformSetProjectionMatrixStack(GLGeometryTransform *geoTransform, GLMatrixStack *mProjection) {
    geoTransform->_mProjection = mProjection;
}

void glGeometryTransformSetMatrixStacks(GLGeometryTransform *geoTransform, GLMatrixStack *mModelView, GLMatrixStack *mProjection) {
    geoTransform->_mModelView = mModelView;
    geoTransform->_mProjection = mProjection;
}

const M3DMatrix44f *glGeometryTransformGetModelViewProjectionMatrix(GLGeometryTransform *geoTransform) {
    M3DMatrix44f *matrixProjection;
    M3DMatrix44f *matrixModelView;
    
    matrixProjection = glMatrixStackGetMatrix(geoTransform->_mProjection);
    matrixModelView = glMatrixStackGetMatrix(geoTransform->_mModelView);
    
    
    m3dMatrixMultiply44f(geoTransform->_mModelViewProjection,
                         (const float *)matrixProjection,
                         (const float *)matrixModelView);
    
    return &geoTransform->_mModelViewProjection;
}

const M3DMatrix44f *glGeometryTransformGetModelViewMatrix(GLGeometryTransform *geoTransform) {
    return glMatrixStackGetMatrix(geoTransform->_mModelView);
}

const M3DMatrix44f *glGeometryTransformGetProjectionMatrix(GLGeometryTransform *geoTransform) {
    return glMatrixStackGetMatrix(geoTransform->_mProjection);
}

const M3DMatrix33f *glGeometryTransformGetNormalMatrix(GLGeometryTransform *geoTransform, bool bNormalize) {
    m3dExtractRotationMatrix33f(geoTransform->_mNormalMatrix,
                                (const float *) glGeometryTransformGetModelViewMatrix(geoTransform));
    if (bNormalize) {
        m3dNormalizeVector3f(&geoTransform->_mNormalMatrix[0]);
        m3dNormalizeVector3f(&geoTransform->_mNormalMatrix[3]);
        m3dNormalizeVector3f(&geoTransform->_mNormalMatrix[6]);
    }
    
    return &geoTransform->_mNormalMatrix;
}



