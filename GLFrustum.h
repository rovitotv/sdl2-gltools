/*
GLFrustum.h

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright (c) 2013, Todd V. Rovito
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef GLFrustum_h
#define GLFrustum_h

#include <math3d.h>
#include <stdbool.h>
#include "GLFrame.h"

struct GLFrustumStruct {
    // The projection matrix for this frustum
    M3DMatrix44f projMatrix;
    
    // Untransformed corners of the frustum
    M3DVector4f  nearUL, nearLL, nearUR, nearLR;
    M3DVector4f  farUL,  farLL,  farUR,  farLR;
    
    // Transformed corners of Frustum
    M3DVector4f  nearULT, nearLLT, nearURT, nearLRT;
    M3DVector4f  farULT,  farLLT,  farURT,  farLRT;
    
    // Base and Transformed plane equations
    M3DVector4f nearPlane, farPlane, leftPlane, rightPlane;
    M3DVector4f topPlane, bottomPlane;
};

typedef struct GLFrustumStruct GLFrustum;

void glFrustumInitDefault(GLFrustum *frustum);
void glFrustumInitOrthographic(GLFrustum *frustum, float xMin, float xMax, float yMin, float yMax, float zMin, float zMax);
void glFrustumInitPerspective(GLFrustum *frustum, float fFov, float fAspect, float fNear, float fFar);

void glFrustumSetOrthographic(GLFrustum *frustum, float xMin, float xMax, float yMin, float yMax, float zMin, float zMax);
void glFrustumSetPerspective(GLFrustum *frustum, float fFov, float fAspect, float fNear, float fFar);

void glFrustumTransform(GLFrustum *frustum, GLFrame *camera);

const M3DMatrix44f *glFrustumGetProjectionMatrix(GLFrustum *frustum);

// Test a point against all frustum planes. A negative distance for any
// single plane means it is outside the frustum. The radius value allows
// to test for a point (radius = 0), or a sphere. Possibly there might
// be some gain in an alternative function that saves the addition of
// zero in this case.
// Returns false if it is not in the frustum, true if it intersects
// the Frustum.
bool glFrustumTestSphere(GLFrustum *frustum, M3DVector3f vPoint, float fRadius);
bool glFrustumTestSpherePoint(GLFrustum *frustum, float x, float y, float z, float fRadius);




#endif
