/*
GLFrame.c

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright Todd V. Rovito 2013
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include "GLFrame.h"

void glFrameInit(GLFrame *frame) {
    // at origin
    frame->vOrigin[0] = 0.0f; frame->vOrigin[1] = 0.0f; frame->vOrigin[2] = 0.0f;
    
    // up is up (+Y)
    frame->vUp[0] = 0.0f; frame->vUp[1] = 1.0f; frame->vUp[2] = 0.0f;
    
    // forward is -Z (default OpenGL)
    frame->vForward[0] = 0.0f; frame->vForward[1] = 0.0f; frame->vForward[2] = -1.0f;
}

void glFrameSetOrigin(GLFrame *frame, const M3DVector3f vPoint) {
    m3dCopyVector3f(frame->vOrigin, vPoint);
}

void glFrameSetOriginFromPoints(GLFrame *frame, float x, float y, float z) {
    frame->vOrigin[0] = x;
    frame->vOrigin[1] = y;
    frame->vOrigin[2] = z;
}

void glFrameGetOrigin(GLFrame *frame, M3DVector3f vPoint) {
    m3dCopyVector3f(vPoint, frame->vOrigin);
}

float glFrameGetOriginX(GLFrame *frame) {
    return frame->vOrigin[0];
}

float glFrameGetOriginY(GLFrame *frame) {
    return frame->vOrigin[1];
    
}

float glFrameGetOriginZ(GLFrame *frame) {
    return frame->vOrigin[2];
}


void glFrameSetForwardVector(GLFrame *frame, const M3DVector3f vDirection) {
    m3dCopyVector3f(frame->vForward, vDirection);
}

void glFrameSetForwardVectorFromPoints(GLFrame *frame, float x, float y, float z) {
    frame->vForward[0] = x;
    frame->vForward[1] = y;
    frame->vForward[2] = z;
}

void glFrameGetForwardVector(GLFrame *frame, M3DVector3f vVector) {
    m3dCopyVector3f(vVector, frame->vForward);
}

void glFrameSetUpVector(GLFrame *frame, const M3DVector3f vDirection) {
    m3dCopyVector3f(frame->vUp, vDirection);
}

void glFrameSetUpVectorFromPoints(GLFrame *frame, float x, float y, float z) {
    frame->vUp[0] = x;
    frame->vUp[1] = y;
    frame->vUp[2] = z;
}

void glFrameGetUpVector(GLFrame *frame, M3DVector3f vVector) {
    m3dCopyVector3f(vVector, frame->vUp);
}

void glFrameGetZAxis(GLFrame *frame, M3DVector3f vVector) {
    glFrameGetForwardVector(frame, vVector);
}

void glFrameGetYAxis(GLFrame *frame, M3DVector3f vVector) {
    glFrameGetUpVector(frame, vVector);
}

void glFrameGetXAxis(GLFrame *frame, M3DVector3f vVector) {
    m3dCrossProduct3f(vVector, frame->vUp, frame->vForward);
}

// move forward (along Z axis)
void glFrameMoveForward(GLFrame *frame, float fDelta) {
    frame->vOrigin[0] += frame->vForward[0] * fDelta;
    frame->vOrigin[1] += frame->vForward[1] * fDelta;
    frame->vOrigin[2] += frame->vForward[2] * fDelta;
}

// move along the y axis
void glFrameMoveUp(GLFrame *frame, float fDelta) {
    frame->vOrigin[0] += frame->vUp[0] * fDelta;
    frame->vOrigin[1] += frame->vUp[1] * fDelta;
    frame->vOrigin[2] += frame->vUp[2] * fDelta;
}

// move along the x axis
void glFrameMoveRight(GLFrame *frame, float fDelta) {
    M3DVector3f vCross;
    m3dCrossProduct3f(vCross, frame->vUp, frame->vForward);
    
    frame->vOrigin[0] += vCross[0] * fDelta;
    frame->vOrigin[1] += vCross[1] * fDelta;
    frame->vOrigin[2] += vCross[2] * fDelta;
}

// translate along orthonormal axis .... world or local
void glFrameTranslateWorld(GLFrame *frame, float x, float y, float z) {
    frame->vOrigin[0] += x;
    frame->vOrigin[1] += y;
    frame->vOrigin[2] += z;
}

void glFrameTranslateLocal(GLFrame *frame, float x, float y, float z) {
    glFrameMoveForward(frame, z);
    glFrameMoveUp(frame, y);
    glFrameMoveRight(frame, x);
}

void glFrameGetMatrix(GLFrame *frame, M3DMatrix44f matrix, bool bRotationOnly) {
    // Calculate the right side (x) vector, drop it right into the matrix
    M3DVector3f vXAxis;
    m3dCrossProduct3f(vXAxis, frame->vUp, frame->vForward);
    
    // Set matrix column does not fill in the fourth value...
    m3dSetMatrixColumn44f(matrix, vXAxis, 0);
    matrix[3] = 0.0f;
    
    // Y Column
    m3dSetMatrixColumn44f(matrix, frame->vUp, 1);
    matrix[7] = 0.0f;
    
    // Z Column
    m3dSetMatrixColumn44f(matrix, frame->vForward, 2);
    matrix[11] = 0.0f;
    
    // Translation (already done)
    if(bRotationOnly == true)
    {
        matrix[12] = 0.0f;
        matrix[13] = 0.0f;
        matrix[14] = 0.0f;
    }
    else
        m3dSetMatrixColumn44f(matrix, frame->vOrigin, 3);
    
    matrix[15] = 1.0f;
}

void glFrameGetCameraMatrix(GLFrame *frame, M3DMatrix44f m, bool bRotationOnly) {
    M3DVector3f x, z;
    
    // Make rotation matrix
    // Z vector is reversed
    z[0] = -frame->vForward[0];
    z[1] = -frame->vForward[1];
    z[2] = -frame->vForward[2];
    
    // X vector = Y cross Z
    m3dCrossProduct3f(x, frame->vUp, z);
    
    // Matrix has no translation information and is
    // transposed.... (rows instead of columns)
#define M(row,col)  m[col*4+row]
    M(0, 0) = x[0];
    M(0, 1) = x[1];
    M(0, 2) = x[2];
    M(0, 3) = 0.0;
    M(1, 0) = frame->vUp[0];
    M(1, 1) = frame->vUp[1];
    M(1, 2) = frame->vUp[2];
    M(1, 3) = 0.0;
    M(2, 0) = z[0];
    M(2, 1) = z[1];
    M(2, 2) = z[2];
    M(2, 3) = 0.0;
    M(3, 0) = 0.0;
    M(3, 1) = 0.0;
    M(3, 2) = 0.0;
    M(3, 3) = 1.0;
#undef M
    
    
    if(bRotationOnly)
        return;
    
    // Apply translation too
    M3DMatrix44f trans, M;
    m3dTranslationMatrix44f(trans, -frame->vOrigin[0],
                            -frame->vOrigin[1], -frame->vOrigin[2]);
    
    m3dMatrixMultiply44f(M, m, trans);
    
    // Copy result back into m
    memcpy(m, M, sizeof(float)*16);
}

void glFrameRotateLocalY(GLFrame *frame, float fAngle) {
    M3DMatrix44f rotMat;
    
    // Just Rotate around the up vector
    // Create a rotation matrix around my Up (Y) vector
    m3dRotationMatrix44f(rotMat, fAngle,
                        frame->vUp[0], frame->vUp[1], frame->vUp[2]);
    
    M3DVector3f newVect;
    
    // Rotate forward pointing vector (inlined 3x3 transform)
    newVect[0] = rotMat[0] * frame->vForward[0] + rotMat[4] * frame->vForward[1] + rotMat[8] *  frame->vForward[2];
    newVect[1] = rotMat[1] * frame->vForward[0] + rotMat[5] * frame->vForward[1] + rotMat[9] *  frame->vForward[2];
    newVect[2] = rotMat[2] * frame->vForward[0] + rotMat[6] * frame->vForward[1] + rotMat[10] * frame->vForward[2];
    m3dCopyVector3f(frame->vForward, newVect);
}

void glFrameRotateLocalZ(GLFrame *frame, float fAngle) {
    M3DMatrix44f rotMat;
    
    // Only the up vector needs to be rotated
    m3dRotationMatrix44f(rotMat, fAngle,
                        frame->vForward[0], frame->vForward[1],
                         frame->vForward[2]);
    
    M3DVector3f newVect;
    newVect[0] = rotMat[0] * frame->vUp[0] + rotMat[4] * frame->vUp[1] + rotMat[8] *  frame->vUp[2];
    newVect[1] = rotMat[1] * frame->vUp[0] + rotMat[5] * frame->vUp[1] + rotMat[9] *  frame->vUp[2];
    newVect[2] = rotMat[2] * frame->vUp[0] + rotMat[6] * frame->vUp[1] + rotMat[10] * frame->vUp[2];
    m3dCopyVector3f(frame->vUp, newVect);
}

void glFrameRotateLocalX(GLFrame *frame, float fAngle) {
    M3DMatrix33f rotMat;
    M3DVector3f  localX;
    M3DVector3f  rotVec;
    
    // Get the local X axis
    m3dCrossProduct3f(localX, frame->vUp, frame->vForward);
    
    // Make a Rotation Matrix
    m3dRotationMatrix33f(rotMat, fAngle, localX[0], localX[1], localX[2]);
    
    // Rotate Y, and Z
    m3dRotateVectorf(rotVec, frame->vUp, rotMat);
    m3dCopyVector3f(frame->vUp, rotVec);
    
    m3dRotateVectorf(rotVec, frame->vForward, rotMat);
    m3dCopyVector3f(frame->vForward, rotVec);
}

void glFrameNormalize(GLFrame *frame) {
    M3DVector3f vCross;
    
    // Calculate cross product of up and forward vectors
    m3dCrossProduct3f(vCross, frame->vUp, frame->vForward);
    
    // Use result to recalculate forward vector
    m3dCrossProduct3f(frame->vForward, vCross, frame->vUp);
    
    // Also check for unit length...
    m3dNormalizeVector3f(frame->vUp);
    m3dNormalizeVector3f(frame->vForward);
}

void glFrameRotateWorld(GLFrame *frame, float fAngle, float x, float y, float z) {
    M3DMatrix44f rotMat;
    
    // Create the Rotation matrix
    m3dRotationMatrix44f(rotMat, fAngle, x, y, z);
    
    M3DVector3f newVect;
    
    // Transform the up axis (inlined 3x3 rotation)
    newVect[0] = rotMat[0] * frame->vUp[0] + rotMat[4] * frame->vUp[1] + rotMat[8] *  frame->vUp[2];
    newVect[1] = rotMat[1] * frame->vUp[0] + rotMat[5] * frame->vUp[1] + rotMat[9] *  frame->vUp[2];
    newVect[2] = rotMat[2] * frame->vUp[0] + rotMat[6] * frame->vUp[1] + rotMat[10] * frame->vUp[2];
    m3dCopyVector3f(frame->vUp, newVect);
    
    // Transform the forward axis
    newVect[0] = rotMat[0] * frame->vForward[0] + rotMat[4] * frame->vForward[1] + rotMat[8] *  frame->vForward[2];
    newVect[1] = rotMat[1] * frame->vForward[0] + rotMat[5] * frame->vForward[1] + rotMat[9] *  frame->vForward[2];
    newVect[2] = rotMat[2] * frame->vForward[0] + rotMat[6] * frame->vForward[1] + rotMat[10] * frame->vForward[2];
    m3dCopyVector3f(frame->vForward, newVect);
}

void glFrameRotateLocal(GLFrame *frame, float fAngle, float x, float y, float z) {
    M3DVector3f vWorldVect;
    M3DVector3f vLocalVect;
    m3dLoadVector3f(vLocalVect, x, y, z);
    
    glFrameLocalToWorld(frame, vLocalVect, vWorldVect, true);
    glFrameRotateWorld(frame, fAngle, vWorldVect[0], vWorldVect[1], vWorldVect[2]);
}

void glFrameLocalToWorld(GLFrame *frame, const M3DVector3f vLocal, M3DVector3f vWorld, bool bRotOnly) {
    // Create the rotation matrix based on the vectors
    M3DMatrix44f rotMat;
    
    glFrameGetMatrix(frame, rotMat, true);
    
    // Do the rotation (inline it, and remove 4th column...)
    vWorld[0] = rotMat[0] * vLocal[0] + rotMat[4] * vLocal[1] + rotMat[8] *  vLocal[2];
    vWorld[1] = rotMat[1] * vLocal[0] + rotMat[5] * vLocal[1] + rotMat[9] *  vLocal[2];
    vWorld[2] = rotMat[2] * vLocal[0] + rotMat[6] * vLocal[1] + rotMat[10] * vLocal[2];
    
    // Translate the point
    if(!bRotOnly) {
        vWorld[0] += frame->vOrigin[0];
        vWorld[1] += frame->vOrigin[1];
        vWorld[2] += frame->vOrigin[2];
    }
}

void glFrameWorldToLocal(GLFrame *frame, const M3DVector3f vWorld, M3DVector3f vLocal) {
    ////////////////////////////////////////////////
    // Translate the origin
    M3DVector3f vNewWorld;
    vNewWorld[0] = vWorld[0] - frame->vOrigin[0];
    vNewWorld[1] = vWorld[1] - frame->vOrigin[1];
    vNewWorld[2] = vWorld[2] - frame->vOrigin[2];
    
    // Create the rotation matrix based on the vectors
    M3DMatrix44f rotMat;
    M3DMatrix44f invMat;
    glFrameGetMatrix(frame, rotMat, true);
    
    // Do the rotation based on inverted matrix
    m3dInvertMatrix44f(invMat, rotMat);
    
    vLocal[0] = invMat[0] * vNewWorld[0] + invMat[4] * vNewWorld[1] + invMat[8] *  vNewWorld[2];
    vLocal[1] = invMat[1] * vNewWorld[0] + invMat[5] * vNewWorld[1] + invMat[9] *  vNewWorld[2];
    vLocal[2] = invMat[2] * vNewWorld[0] + invMat[6] * vNewWorld[1] + invMat[10] * vNewWorld[2];
}

void glFrameTransformPoint(GLFrame *frame, M3DVector3f vPointSrc, M3DVector3f vPointDst) {
    M3DMatrix44f m;
    glFrameGetMatrix(frame, m, false);    // Rotate and translate
    vPointDst[0] = m[0] * vPointSrc[0] + m[4] * vPointSrc[1] + m[8] *  vPointSrc[2] + m[12];// * v[3];
    vPointDst[1] = m[1] * vPointSrc[0] + m[5] * vPointSrc[1] + m[9] *  vPointSrc[2] + m[13];// * v[3];
    vPointDst[2] = m[2] * vPointSrc[0] + m[6] * vPointSrc[1] + m[10] * vPointSrc[2] + m[14];// * v[3];
}


void glFrameRotateVector(GLFrame *frame, M3DVector3f vVectorSrc, M3DVector3f vVectorDst) {
    M3DMatrix44f m;
    glFrameGetMatrix(frame, m, true);    // Rotate only
    
    vVectorDst[0] = m[0] * vVectorSrc[0] + m[4] * vVectorSrc[1] + m[8] *  vVectorSrc[2];
    vVectorDst[1] = m[1] * vVectorSrc[0] + m[5] * vVectorSrc[1] + m[9] *  vVectorSrc[2];
    vVectorDst[2] = m[2] * vVectorSrc[0] + m[6] * vVectorSrc[1] + m[10] * vVectorSrc[2];
    
}






