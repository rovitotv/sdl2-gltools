/*
SphereWorld3.c

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright (c) 2013,  Todd V. Rovito
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <GLTools.h>
#include <GLShaderManager.h>
#include <GLFrustum.h>
#include <GLBatch.h>
#include <GLFrame.h>
#include <GLMatrixStack.h>
#include <GLGeometryTransform.h>
#include <StopWatch.h>

#include <math.h>
#include <stdio.h>
#include <SDL.h>
#define PROGRAM_NAME "OpenGL Sphere World 3"
#define NUM_SPHERES 50


/* A simple function that prints a message, the error code returned by SDL,
 *  * and quits the application */
void sdldie(const char *msg)
{
    printf("%s: %s\n", msg, SDL_GetError());
    SDL_Quit();
    exit(1);
}

void checkSDLError(int line)
{
#ifndef NDEBUG
    const char *error = SDL_GetError();
    if (*error != '\0')
    {
        printf("SDL Error: %s\n", error);
        if (line != -1)
            printf(" + line: %i\n", line);
        SDL_ClearError();
    }
#endif
}

//////////////////////////////////////////////////////////////////
// This function does any needed initialization on the rendering
// context. 
void SetupRC(GLuint *uiStockShaders, GLBatch *floorBatch,
             GLTriangle *torusBatch,
             GLTriangle *sphereBatch, GLFrame *spheres)
{
	// Initialze Shader Manager
    glShaderManagerInitializeStockShaders(uiStockShaders);
	
	glEnable(GL_DEPTH_TEST);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
	glToolsSetClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	// This makes a torus
	glToolsMakeTorus(torusBatch, 0.4f, 0.15f, 30, 30);
	
    // This make a sphere
    glToolsMakeSphere(sphereBatch, 0.1f, 26, 13);

    // this makes the floor
    glBatchBegin(floorBatch, GL_LINES, 324, 0);
    for(GLfloat x = -20.0; x <= 20.0f; x+= 0.5) {
        glBatchVertex3f(floorBatch, x, -0.55f, 20.0f);
        glBatchVertex3f(floorBatch, x, -0.55f, -20.0f);
        
        glBatchVertex3f(floorBatch, 20.0f, -0.55f, x);
        glBatchVertex3f(floorBatch, -20.0f, -0.55f, x);
    }
    glBatchEnd(floorBatch);

    // Randomly place the spheres
    for(int i = 0; i < NUM_SPHERES; i++) {
        GLfloat x = ((GLfloat)((rand() % 400) - 200) * 0.1f);
        GLfloat z = ((GLfloat)((rand() % 400) - 200) * 0.1f);
        glFrameSetOriginFromPoints(&spheres[i], x, 0.0f, z);
        // this came from constructor for glFrame
        glFrameSetUpVectorFromPoints(&spheres[i], 0.0f, 1.0f, 0.0f);
        glFrameSetForwardVectorFromPoints(&spheres[i], 0.0f, 0.0f, -1.0f);
    }
}


///////////////////////////////////////////////////
// Screen changes size or is initialized
void ChangeSize(int nWidth, int nHeight, GLFrustum *viewFrustum,
                GLMatrixStack *projectionMatrix,
                GLMatrixStack *modelViewMatrix,
                GLGeometryTransform *transformPipeline)
{
    // Create the projection matrix, and load it on the projection matrix stack
    glFrustumSetPerspective(viewFrustum, 35.0f,
                            (float) nWidth / (float) nHeight, 1.0f, 100.0f);
    glMatrixStackLoadMatrix(projectionMatrix,
                (const float *)glFrustumGetProjectionMatrix(viewFrustum));
    
    // Set the transformation pipeline to use the two matrix stacks
    glGeometryTransformSetMatrixStacks(transformPipeline, modelViewMatrix,
                                       projectionMatrix);
}


// Called to draw scene
void RenderScene(CStopWatch *rotTimer, GLMatrixStack *modelViewMatrix,
                 GLuint *uiStockShaders,
                 GLGeometryTransform *transformPipeline, GLBatch *floorBatch,
                 GLTriangle *torusBatch,
                 GLTriangle *sphereBatch, GLFrame *cameraFrame,
                 GLFrame *spheres)
{
    // Color values
    static GLfloat vFloorColor[] = { 0.0f, 1.0f, 0.0f, 1.0f};
    static GLfloat vTorusColor[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    static GLfloat vSphereColor[] = { 0.0f, 0.0f, 1.0f, 1.0f };
    
    // Time Based animation
	float yRot = stopWatchGetElapsedSeconds(rotTimer) * 60.0f;
    
    
	// Clear the color and depth buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    
    // Save the current modelview matrix (the identity matrix)
    glMatrixStackPushMatrix(modelViewMatrix);
    
    M3DMatrix44f mCamera;
    glFrameGetCameraMatrix(cameraFrame, mCamera, false);
    glMatrixStackPushNewMatrix(modelViewMatrix, mCamera);
    
	// Draw the ground
    glShaderManagerUseStockShaderFlat(uiStockShaders,
        glGeometryTransformGetModelViewProjectionMatrix(transformPipeline),
        vFloorColor);
    glBatchDraw(floorBatch);
    
    
    for(int i = 0; i < NUM_SPHERES; i++) {
        glMatrixStackPushMatrix(modelViewMatrix);
        glMatrixStackMultiplyMatrixFromFrame(modelViewMatrix, &spheres[i]);
        glShaderManagerUseStockShaderFlat(uiStockShaders,
            glGeometryTransformGetModelViewProjectionMatrix(transformPipeline),
            vSphereColor);
        glTriangleBatchDraw(sphereBatch);
        glMatrixStackPopMatrix(modelViewMatrix);
    }
    
    // Draw the spinning Torus
    glMatrixStackTranslate(modelViewMatrix, 0.0f, 0.0f, -2.5f);
    
    // Save the Translation
    glMatrixStackPushMatrix(modelViewMatrix);
    
    // Apply a rotation and draw the torus
    glMatrixStackRotate(modelViewMatrix, yRot, 0.0f, 1.0f, 0.0f);
    glShaderManagerUseStockShaderFlat(uiStockShaders,
        glGeometryTransformGetModelViewProjectionMatrix(transformPipeline),
        vTorusColor);
    glTriangleBatchDraw(torusBatch);
    glMatrixStackPopMatrix(modelViewMatrix); // "Erase" the Rotation from before
    
    // Apply another rotation, followed by a translation, then draw the sphere
    glMatrixStackRotate(modelViewMatrix, yRot * -2.0f, 0.0f, 1.0f, 0.0f);
    glMatrixStackTranslate(modelViewMatrix, 0.8f, 0.0f, 0.0f);
    glShaderManagerUseStockShaderFlat(uiStockShaders,
        glGeometryTransformGetModelViewProjectionMatrix(transformPipeline),
        vSphereColor);
    glTriangleBatchDraw(sphereBatch);
    
	// Restore the previous modleview matrix (the identity matrix)
    glMatrixStackPopMatrix(modelViewMatrix);
    glMatrixStackPopMatrix(modelViewMatrix);
}


// Respond to arrow keys by moving the camera frame of reference
bool SpecialKeys(GLFrame *cameraFrame)
{
    SDL_Event event;
    SDL_Keycode keyPressed;
    float linear = 0.1f;
	float angular = (float) (m3dDegToRad(5.0f));
    bool gameRunning;
    
    gameRunning = true;
    if (SDL_PollEvent(&event))
    {
        if (event.type == SDL_KEYDOWN)
        {
            keyPressed = event.key.keysym.sym;
            
            switch (keyPressed)
            {
                case SDLK_ESCAPE:
                    gameRunning = false;
                    break;
                case SDLK_UP:
                    glFrameMoveForward(cameraFrame, linear);
                    break;
                case SDLK_DOWN:
                    glFrameMoveForward(cameraFrame, -linear);
                    break;
                case SDLK_LEFT:
                    glFrameRotateWorld(cameraFrame, angular, 0.0f, 1.0f, 0.0f);
                    break;
                case SDLK_RIGHT:
                    glFrameRotateWorld(cameraFrame, -angular, 0.0f, 1.0f, 0.0f);
                    break;
            }
        }
    }
    
    return gameRunning;
}

int main(int argc, char* argv[])
{
    GLFrame spheres[NUM_SPHERES];
    //GLShaderManager		shaderManager;			// Shader Manager
    GLuint uiStockShaders[GLT_SHADER_LAST];
    GLMatrixStack		modelViewMatrix;		// Modelview Matrix
    GLMatrixStack		projectionMatrix;		// Projection Matrix
    GLFrustum			viewFrustum;			// View Frustum
    GLGeometryTransform	transformPipeline;		// Geometry Transform Pipeline
    CStopWatch rotTimer;
    GLTriangle	   torusBatch;
    GLBatch		   floorBatch;
    GLTriangle     sphereBatch;
    GLFrame        cameraFrame;
    bool gameRunning;
    
    // init all the objects that we need
    gameRunning = true;
    glBatchInitBatch(&floorBatch);
    glTriangleBatchInit(&torusBatch);
    glTriangleBatchInit(&sphereBatch);
    stopWatchInit(&rotTimer);
    glMatrixStackInit(&modelViewMatrix, 64);
    glMatrixStackInit(&projectionMatrix, 64);
    glFrustumInitDefault(&viewFrustum);
    glFrameInit(&cameraFrame);
    
    SDL_Window *mainwindow;
    SDL_GLContext maincontext;
    
    /* Request opengl 3.2 context.
     * SDL doesn't have the ability to choose which profile at this time of 
     * writing, but it should default to the core profile */
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    
    /* Turn on double buffering with a 24bit Z buffer.
     * You may need to change this to 16 or 32 for your system */
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        sdldie("Unable to initalize SDL");

    
    /* Create our window centered at 512x512 resolution */
    mainwindow = SDL_CreateWindow(PROGRAM_NAME, SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED,
                                  800, 600, SDL_WINDOW_OPENGL |
                                  SDL_WINDOW_SHOWN);
    if (!mainwindow) /* Die if creation failed */
        sdldie("Unable to create window");
    
    checkSDLError(__LINE__);
    
    /* Create our opengl context and attach it to our window */
    maincontext = SDL_GL_CreateContext(mainwindow);
    checkSDLError(__LINE__);
    
    GLenum err = glewInit();
    if (GLEW_OK != err) {
        fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
        return 1;
    }
    
    SetupRC(uiStockShaders, &floorBatch, &torusBatch, &sphereBatch, spheres);
    ChangeSize(800, 600, &viewFrustum, &projectionMatrix, &modelViewMatrix,
               &transformPipeline);
    while (gameRunning) {
        gameRunning = SpecialKeys(&cameraFrame);
        RenderScene(&rotTimer, &modelViewMatrix, uiStockShaders,
                    &transformPipeline, &floorBatch, &torusBatch, &sphereBatch,
                    &cameraFrame, spheres);
        SDL_GL_SwapWindow(mainwindow);
    }
    SDL_GL_DeleteContext(maincontext);
    SDL_DestroyWindow(mainwindow);
    SDL_Quit();
    
    return 0;
}
