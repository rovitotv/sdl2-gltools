/*
Math3d.h

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright Todd V. Rovito 2013
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Header file for the Math3d library. The C-Runtime has math.h, this file and the
// accompanying math3d.cpp are meant to suppliment math.h by adding geometry/math routines
// useful for graphics, simulation, and physics applications (3D stuff).
// This library is meant to be useful on Win32, Mac OS X, various Linux/Unix distros,
// and mobile platforms. Although designed with OpenGL in mind, there are no OpenGL 
// dependencies. Other than standard math routines, the only other outside routine
// used is memcpy (for faster copying of vector arrays).
// Richard S. Wright Jr.

#ifndef _MATH3D_LIBRARY__
#define _MATH3D_LIBRARY__

#include <math.h>
#include <string.h>	// Memcpy lives here on most systems

///////////////////////////////////////////////////////////////////////////////
// Data structures and containers
// Much thought went into how these are declared. Many libraries declare these
// as structures with x, y, z data members. However structure alignment issues
// could limit the portability of code based on such structures, or the binary
// compatibility of data files (more likely) that contain such structures across
// compilers/platforms. Arrays are always tightly packed, and are more efficient 
// for moving blocks of data around (usually).
// Sigh... yes, I probably should use GLfloat, etc. But that requires that we
// always include OpenGL. Since this library is also useful for non-graphical
// applications, I shall risk the wrath of the portability gods...

typedef float	M3DVector2f[2];		// 3D points = 3D Vectors, but we need a 
typedef double	M3DVector2d[2];		// 2D representations sometimes... (x,y) order

typedef float	M3DVector3f[3];		// Vector of three floats (x, y, z)
typedef double	M3DVector3d[3];		// Vector of three doubles (x, y, z)

typedef float	M3DVector4f[4];		// Lesser used... Do we really need these?
typedef double	M3DVector4d[4];		// Yes, occasionaly we do need a trailing w component



// 3x3 matrix - column major. X vector is 0, 1, 2, etc.
//		0	3	6	
//		1	4	7
//		2	5	8
typedef float	M3DMatrix33f[9];		// A 3 x 3 matrix, column major (floats) - OpenGL Style
typedef double	M3DMatrix33d[9];		// A 3 x 3 matrix, column major (doubles) - OpenGL Style


// 4x4 matrix - column major. X vector is 0, 1, 2, etc.
//	0	4	8	12
//	1	5	9	13
//	2	6	10	14
//	3	7	11	15
typedef float M3DMatrix44f[16];		// A 4 X 4 matrix, column major (floats) - OpenGL style
typedef double M3DMatrix44d[16];	// A 4 x 4 matrix, column major (doubles) - OpenGL style


///////////////////////////////////////////////////////////////////////////////
// Useful constants
#define M3D_PI (3.14159265358979323846)
#define M3D_2PI (2.0 * M3D_PI)
#define M3D_PI_DIV_180 (0.017453292519943296)
#define M3D_INV_PI_DIV_180 (57.2957795130823229)


///////////////////////////////////////////////////////////////////////////////
// Useful shortcuts and macros
// Radians are king... but we need a way to swap back and forth for programmers and presentation.
// Leaving these as Macros instead of inline functions, causes constants
// to be evaluated at compile time instead of run time, e.g. m3dDegToRad(90.0)
#define m3dDegToRad(x)	((x)*M3D_PI_DIV_180)
#define m3dRadToDeg(x)	((x)*M3D_INV_PI_DIV_180)

// Hour angles
#define m3dHrToDeg(x)	((x) * (1.0 / 15.0))
#define m3dHrToRad(x)	m3dDegToRad(m3dHrToDeg(x))

#define m3dDegToHr(x)	((x) * 15.0))
#define m3dRadToHr(x)	m3dDegToHr(m3dRadToDeg(x))


unsigned int m3dIsPOW2(unsigned int iValue);


///////////////////////////////////////////////////////////////////////////////
// Inline accessor functions (Macros) for people who just can't count to 3 or 4
// Really... you should learn to count before you learn to program ;-)
// 0 = x
// 1 = y
// 2 = z
// 3 = w
#define	m3dGetVectorX(v) (v[0])
#define m3dGetVectorY(v) (v[1])
#define m3dGetVectorZ(v) (v[2])
#define m3dGetVectorW(v) (v[3])

#define m3dSetVectorX(v, x)	((v)[0] = (x))
#define m3dSetVectorY(v, y)	((v)[1] = (y))
#define m3dSetVectorZ(v, z)	((v)[2] = (z))
#define m3dSetVectorW(v, w)	((v)[3] = (w))

///////////////////////////////////////////////////////////////////////////////
// Load Vector with (x, y, z, w).
void m3dLoadVector2f(M3DVector2f v, const float x, const float y);
void m3dLoadVector2d(M3DVector2d v, const float x, const float y);
void m3dLoadVector3f(M3DVector3f v, const float x, const float y, const float z);
void m3dLoadVector3d(M3DVector3d v, const double x, const double y, const double z);
void m3dLoadVector4f(M3DVector4f v, const float x, const float y, const float z, const float w);
void m3dLoadVector4d(M3DVector4d v, const double x, const double y, const double z, const double w);


////////////////////////////////////////////////////////////////////////////////
// Copy vector src into vector dst
void	m3dCopyVector2f(M3DVector2f dst, const M3DVector2f src);
void	m3dCopyVector2d(M3DVector2d dst, const M3DVector2d src);

void	m3dCopyVector3f(M3DVector3f dst, const M3DVector3f src);
void	m3dCopyVector3d(M3DVector3d dst, const M3DVector3d src);

void	m3dCopyVector4f(M3DVector4f dst, const M3DVector4f src);
void	m3dCopyVector4d(M3DVector4d dst, const M3DVector4d src);


////////////////////////////////////////////////////////////////////////////////
// Add Vectors (r, a, b) r = a + b
void m3dAddVectors2f(M3DVector2f r, const M3DVector2f a, const M3DVector2f b);
void m3dAddVectors2d(M3DVector2d r, const M3DVector2d a, const M3DVector2d b);

void m3dAddVectors3f(M3DVector3f r, const M3DVector3f a, const M3DVector3f b);
void m3dAddVectors3d(M3DVector3d r, const M3DVector3d a, const M3DVector3d b);

void m3dAddVectors4f(M3DVector4f r, const M3DVector4f a, const M3DVector4f b);
void m3dAddVectors4d(M3DVector4d r, const M3DVector4d a, const M3DVector4d b);

////////////////////////////////////////////////////////////////////////////////
// Subtract Vectors (r, a, b) r = a - b
void m3dSubtractVectors2f(M3DVector2f r, const M3DVector2f a, const M3DVector2f b);
void m3dSubtractVectors2d(M3DVector2d r, const M3DVector2d a, const M3DVector2d b);

void m3dSubtractVectors3f(M3DVector3f r, const M3DVector3f a, const M3DVector3f b);
void m3dSubtractVectors3d(M3DVector3d r, const M3DVector3d a, const M3DVector3d b);

void m3dSubtractVectors4f(M3DVector4f r, const M3DVector4f a, const M3DVector4f b);
void m3dSubtractVectors4d(M3DVector4d r, const M3DVector4d a, const M3DVector4d b);



///////////////////////////////////////////////////////////////////////////////////////
// Scale Vectors (in place)
void m3dScaleVector2f(M3DVector2f v, const float scale);
void m3dScaleVector2d(M3DVector2d v, const double scale);

void m3dScaleVector3f(M3DVector3f v, const float scale);
void m3dScaleVector3d(M3DVector3d v, const double scale);

void m3dScaleVector4f(M3DVector4f v, const float scale);
void m3dScaleVector4d(M3DVector4d v, const double scale);


//////////////////////////////////////////////////////////////////////////////////////
// Cross Product
// u x v = result
// 3 component vectors only.
void m3dCrossProduct3f(M3DVector3f result, const M3DVector3f u, const M3DVector3f v);

void m3dCrossProduct3d(M3DVector3d result, const M3DVector3d u, const M3DVector3d v);

//////////////////////////////////////////////////////////////////////////////////////
// Dot Product, only for three component vectors
// return u dot v
float m3dDotProduct3f(const M3DVector3f u, const M3DVector3f v);

double m3dDotProduct3d(const M3DVector3d u, const M3DVector3d v);

//////////////////////////////////////////////////////////////////////////////////////
// Angle between vectors, only for three component vectors. Angle is in radians...
float m3dGetAngleBetweenVectors3f(const M3DVector3f u, const M3DVector3f v);

double m3dGetAngleBetweenVectors3d(const M3DVector3d u, const M3DVector3d v);
//////////////////////////////////////////////////////////////////////////////////////
// Get Square of a vectors length
// Only for three component vectors
float m3dGetVectorLengthSquared3f(const M3DVector3f u);
double m3dGetVectorLengthSquared3d(const M3DVector3d u);

//////////////////////////////////////////////////////////////////////////////////////
// Get lenght of vector
// Only for three component vectors.
float m3dGetVectorLength3f(const M3DVector3f u);

double m3dGetVectorLength3d(const M3DVector3d u);

//////////////////////////////////////////////////////////////////////////////////////
// Normalize a vector
// Scale a vector to unit length. Easy, just scale the vector by it's length
void m3dNormalizeVector3f(M3DVector3f u);

void m3dNormalizeVector3d(M3DVector3d u);

//////////////////////////////////////////////////////////////////////////////////////
// Get the distance between two points. The distance between two points is just
// the magnitude of the difference between two vectors
// Located in math.cpp
float m3dGetDistanceSquared3f(const M3DVector3f u, const M3DVector3f v);
double m3dGetDistanceSquared3d(const M3DVector3d u, const M3DVector3d v);

double m3dGetDistance3d(const M3DVector3d u, const M3DVector3d v);

float m3dGetDistance3f(const M3DVector3f u, const M3DVector3f v);

float m3dGetMagnitudeSquared3f(const M3DVector3f u); 
double m3dGetMagnitudeSquared3d(const M3DVector3d u); 

float m3dGetMagnitude3f(const M3DVector3f u);
double m3dGetMagnitude3d(const M3DVector3d u);

	

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Matrix functions
// Both floating point and double precision 3x3 and 4x4 matricies are supported.
// No support is included for arbitrarily dimensioned matricies on purpose, since
// the 3x3 and 4x4 matrix routines are the most common for the purposes of this
// library. Matrices are column major, like OpenGL matrices.
// Unlike the vector functions, some of these are going to have to not be inlined,
// although many will be.

// Copy Matrix
// Brain-dead memcpy
void m3dCopyMatrix33f(M3DMatrix33f dst, const M3DMatrix33f src);

void m3dCopyMatrix33d(M3DMatrix33d dst, const M3DMatrix33d src);

void m3dCopyMatrix44f(M3DMatrix44f dst, const M3DMatrix44f src);

void m3dCopyMatrix44d(M3DMatrix44d dst, const M3DMatrix44d src);

// LoadIdentity
// Implemented in Math3d.cpp
void m3dLoadIdentity33f(M3DMatrix33f m);
void m3dLoadIdentity33d(M3DMatrix33d m);
void m3dLoadIdentity44f(M3DMatrix44f m);
void m3dLoadIdentity44d(M3DMatrix44d m);

/////////////////////////////////////////////////////////////////////////////
// Get/Set Column.
void m3dGetMatrixColumn33f(M3DVector3f dst, const M3DMatrix33f src, const int column);
void m3dGetMatrixColumn33d(M3DVector3d dst, const M3DMatrix33d src, const int column);
void m3dSetMatrixColumn33f(M3DMatrix33f dst, const M3DVector3f src, const int column);
void m3dSetMatrixColumn33d(M3DMatrix33d dst, const M3DVector3d src, const int column);
void m3dGetMatrixColumn44f(M3DVector4f dst, const M3DMatrix44f src, const int column);
void m3dGetMatrixColumn44d(M3DVector4d dst, const M3DMatrix44d src, const int column);
void m3dSetMatrixColumn44f(M3DMatrix44f dst, const M3DVector4f src, const int column);
void m3dSetMatrixColumn44d(M3DMatrix44d dst, const M3DVector4d src, const int column);


///////////////////////////////////////////////////////////////////////////////
// Extract a rotation matrix from a 4x4 matrix
// Extracts the rotation matrix (3x3) from a 4x4 matrix
void m3dExtractRotationMatrix33f(M3DMatrix33f dst, const M3DMatrix44f src);

// Ditto above, but for doubles
void m3dExtractRotationMatrix33d(M3DMatrix33d dst, const M3DMatrix44d src);
// Inject Rotation (3x3) into a full 4x4 matrix...
void m3dInjectRotationMatrix44f(M3DMatrix44f dst, const M3DMatrix33f src);

// Ditto above for doubles
void m3dInjectRotationMatrix44d(M3DMatrix44d dst, const M3DMatrix33d src);

////////////////////////////////////////////////////////////////////////////////
// MultMatrix
// Implemented in Math.cpp
void m3dMatrixMultiply44f(M3DMatrix44f product, const M3DMatrix44f a, const M3DMatrix44f b);
void m3dMatrixMultiply44d(M3DMatrix44d product, const M3DMatrix44d a, const M3DMatrix44d b);
void m3dMatrixMultiply33f(M3DMatrix33f product, const M3DMatrix33f a, const M3DMatrix33f b);
void m3dMatrixMultiply33d(M3DMatrix33d product, const M3DMatrix33d a, const M3DMatrix33d b);


// Transform - Does rotation and translation via a 4x4 matrix. Transforms
// a point or vector.
// By-the-way __inline means I'm asking the compiler to do a cost/benefit analysis. If 
// these are used frequently, they may not be inlined to save memory. I'm experimenting
// with this....
// Just transform a 3 compoment vector
void m3dTransformVector3f(M3DVector3f vOut, const M3DVector3f v, const M3DMatrix44f m);

// Ditto above, but for doubles
void m3dTransformVector3d(M3DVector3d vOut, const M3DVector3d v, const M3DMatrix44d m);

// Full four component transform
void m3dTransformVector4f(M3DVector4f vOut, const M3DVector4f v, const M3DMatrix44f m);

// Ditto above, but for doubles
void m3dTransformVector4d(M3DVector4d vOut, const M3DVector4d v, const M3DMatrix44d m);



// Just do the rotation, not the translation... this is usually done with a 3x3
// Matrix.
void m3dRotateVectorf(M3DVector3f vOut, const M3DVector3f p, const M3DMatrix33f m);

// Ditto above, but for doubles
void m3dRotateVectord(M3DVector3d vOut, const M3DVector3d p, const M3DMatrix33d m);


// Create a Scaling Matrix
void m3dScaleMatrix33f(M3DMatrix33f m, float xScale, float yScale, float zScale);
void m3dScaleMatrix33Vectorf(M3DMatrix33f m, const M3DVector3f vScale);
void m3dScaleMatrix33d(M3DMatrix33d m, double xScale, double yScale, double zScale);
void m3dScaleMatrix33Vectord(M3DMatrix33d m, const M3DVector3d vScale);
void m3dScaleMatrix44f(M3DMatrix44f m, float xScale, float yScale, float zScale);
void m3dScaleMatrix44Vectorf(M3DMatrix44f m, const M3DVector3f vScale);
void m3dScaleMatrix44d(M3DMatrix44d m, double xScale, double yScale, double zScale);
void m3dScaleMatrix44Vectord(M3DMatrix44d m, const M3DVector3d vScale);

	
void m3dMakePerspectiveMatrix(M3DMatrix44f mProjection, float fFov, float fAspect, float zMin, float zMax);
void m3dMakeOrthographicMatrix(M3DMatrix44f mProjection, float xMin, float xMax, float yMin, float yMax, float zMin, float zMax);


// Create a Rotation matrix
// Implemented in math3d.cpp
void m3dRotationMatrix33f(M3DMatrix33f m, float angle, float x, float y, float z);
void m3dRotationMatrix33d(M3DMatrix33d m, double angle, double x, double y, double z);
void m3dRotationMatrix44f(M3DMatrix44f m, float angle, float x, float y, float z);
void m3dRotationMatrix44d(M3DMatrix44d m, double angle, double x, double y, double z);

// Create a Translation matrix. Only 4x4 matrices have translation components
void m3dTranslationMatrix44f(M3DMatrix44f m, float x, float y, float z);

void m3dTranslationMatrix44d(M3DMatrix44d m, double x, double y, double z);

void m3dInvertMatrix44f(M3DMatrix44f mInverse, const M3DMatrix44f m);
void m3dInvertMatrix44d(M3DMatrix44d mInverse, const M3DMatrix44d m);

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Other Miscellaneous functions

// Find a normal from three points
// Implemented in math3d.cpp
void m3dFindNormalf(M3DVector3f result, const M3DVector3f point1, const M3DVector3f point2,
							const M3DVector3f point3);
void m3dFindNormald(M3DVector3d result, const M3DVector3d point1, const M3DVector3d point2,
							const M3DVector3d point3);


// Calculates the signed distance of a point to a plane
float m3dGetDistanceToPlanef(const M3DVector3f point, const M3DVector4f plane);
double m3dGetDistanceToPlaned(const M3DVector3d point, const M3DVector4d plane);


// Get plane equation from three points
void m3dGetPlaneEquationf(M3DVector4f planeEq, const M3DVector3f p1, const M3DVector3f p2, const M3DVector3f p3);
void m3dGetPlaneEquationd(M3DVector4d planeEq, const M3DVector3d p1, const M3DVector3d p2, const M3DVector3d p3);

// Determine if a ray intersects a sphere
// Return value is < 0 if the ray does not intersect
// Return value is 0.0 if ray is tangent
// Positive value is distance to the intersection point
double m3dRaySphereTestd(const M3DVector3d point, const M3DVector3d ray, const M3DVector3d sphereCenter, double sphereRadius);
float m3dRaySphereTestf(const M3DVector3f point, const M3DVector3f ray, const M3DVector3f sphereCenter, float sphereRadius);


///////////////////////////////////////////////////////////////////////////////////////////////////////
// Faster (and one shortcut) replacements for gluProject
void m3dProjectXY( M3DVector2f vPointOut, const M3DMatrix44f mModelView, const M3DMatrix44f mProjection, const int iViewPort[4], const M3DVector3f vPointIn);    
void m3dProjectXYZ(M3DVector3f vPointOut, const M3DMatrix44f mModelView, const M3DMatrix44f mProjection, const int iViewPort[4], const M3DVector3f vPointIn);


//////////////////////////////////////////////////////////////////////////////////////////////////
// This function does a three dimensional Catmull-Rom "spline" interpolation between p1 and p2
void m3dCatmullRomf(M3DVector3f vOut, const M3DVector3f vP0, const M3DVector3f vP1, const M3DVector3f vP2, const M3DVector3f vP3, float t);
void m3dCatmullRomd(M3DVector3d vOut, const M3DVector3d vP0, const M3DVector3d vP1, const M3DVector3d vP2, const M3DVector3d vP3, double t);

//////////////////////////////////////////////////////////////////////////////////////////////////
// Compare floats and doubles... 
int m3dCloseEnoughf(const float fCandidate, const float fCompare, const float fEpsilon);
int m3dCloseEnoughd(const double dCandidate, const double dCompare, const double dEpsilon);
 
////////////////////////////////////////////////////////////////////////////
// Used for normal mapping. Finds the tangent bases for a triangle...
// Only a floating point implementation is provided. This has no practical use as doubles.
void m3dCalculateTangentBasisf(M3DVector3f vTangent, const M3DVector3f pvTriangle[3], const M3DVector2f pvTexCoords[3], const M3DVector3f N);

////////////////////////////////////////////////////////////////////////////
// Smoothly step between 0 and 1 between edge1 and edge 2
double m3dSmoothStepd(const double edge1, const double edge2, const double x);
float m3dSmoothStepf(const float edge1, const float edge2, const float x);

/////////////////////////////////////////////////////////////////////////////
// Planar shadow Matrix
void m3dMakePlanarShadowMatrixd(M3DMatrix44d proj, const M3DVector4d planeEq, const M3DVector3d vLightPos);
void m3dMakePlanarShadowMatrixf(M3DMatrix44f proj, const M3DVector4f planeEq, const M3DVector3f vLightPos);

/////////////////////////////////////////////////////////////////////////////
// Closest point on a ray to another point in space
double m3dClosestPointOnRayd(M3DVector3d vPointOnRay, const M3DVector3d vRayOrigin, const M3DVector3d vUnitRayDir,
							const M3DVector3d vPointInSpace);

float m3dClosestPointOnRayf(M3DVector3f vPointOnRay, const M3DVector3f vRayOrigin, const M3DVector3f vUnitRayDir,
							const M3DVector3f vPointInSpace);

#endif
