/*
GLTriangleBatch.c

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

See LICENSE.txt for full details of the MPL license.

Copyright Todd V. Rovito 2013
https://bitbucket.org/rovitotv/sdl2-gltools


The Open GL Super Bible has the following license and must be followed:

Copyright (c) 2009, Richard S. Wright Jr.
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
 
Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.
 
Neither the name of Richard S. Wright Jr. nor the names of other contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include "GLTriangleBatch.h"

///////////////////////////////////////////////////////////
// Constructor, does what constructors do... set everything to zero or NULL
void glTriangleBatchInit(GLTriangle *glTriangle) {
    glTriangle->pIndexes = NULL;
    glTriangle->pVerts = NULL;
    glTriangle->pNorms = NULL;
    glTriangle->pTexCoords = NULL;
    
    glTriangle->nMaxIndexes = 0;
    glTriangle->nNumIndexes = 0;
    glTriangle->nNumVerts = 0;
}

////////////////////////////////////////////////////////////
// Free any dynamically allocated memory. For those C programmers
// coming to C++, it is perfectly valid to delete a NULL pointer.
void glTriangleBatchDestroy(GLTriangle *glTriangle) {
    // Just in case these still are allocated when the object is destroyed
    if (glTriangle->pIndexes)
        free(glTriangle->pIndexes);
    if (glTriangle->pVerts)
        free(glTriangle->pVerts);
    if (glTriangle->pNorms)
        free(glTriangle->pNorms);
    if (glTriangle->pTexCoords)
        free(glTriangle->pTexCoords);
    
    // Delete buffer objects
    glDeleteBuffers(4, glTriangle->bufferObjects);
    
#ifndef OPENGL_ES
    glDeleteVertexArrays(1, &glTriangle->vertexArrayBufferObject);
#endif
}

////////////////////////////////////////////////////////////
// Start assembling a mesh. You need to specify a maximum amount
// of indexes that you expect. The EndMesh will clean up any uneeded
// memory. This is far better than shreading your heap with STL containers...
// At least that's my humble opinion.
void glTriangleBatchBeginMesh(GLTriangle *glTriangle, unsigned int nMaxVerts) {
    // Just in case this gets called more than once...
    if (glTriangle->pIndexes)
        free(glTriangle->pIndexes);
    if (glTriangle->pVerts)
        free(glTriangle->pVerts);
    if (glTriangle->pNorms)
        free(glTriangle->pNorms);
    if (glTriangle->pTexCoords)
        free(glTriangle->pTexCoords);
    
    glTriangle->nMaxIndexes = nMaxVerts;
    glTriangle->nNumIndexes = 0;
    glTriangle->nNumVerts = 0;
    
    // Allocate new blocks. In reality, the other arrays will be
    // much shorter than the index array
    glTriangle->pIndexes = malloc(sizeof(GLushort) * glTriangle->nMaxIndexes);
    glTriangle->pVerts = malloc(sizeof(M3DVector3f) * glTriangle->nMaxIndexes);
    glTriangle->pNorms = malloc(sizeof(M3DVector3f) * glTriangle->nMaxIndexes);
    glTriangle->pTexCoords = malloc(sizeof(M3DVector2f) * glTriangle->nMaxIndexes);
}

/////////////////////////////////////////////////////////////////
// Add a triangle to the mesh. This searches the current list for identical
// (well, almost identical - these are floats you know...) verts. If one is found, it
// is added to the index array. If not, it is added to both the index array and the vertex
// array grows by one as well.
void glTriangleBatchAddTriangle(GLTriangle *glTriangle, M3DVector3f verts[3], M3DVector3f vNorms[3], M3DVector2f vTexCoords[3]) {
    const  float e = 0.00001f; // How small a difference to equate
    
    // First thing we do is make sure the normals are unit length!
    // It's almost always a good idea to work with pre-normalized normals
    m3dNormalizeVector3f(vNorms[0]);
    m3dNormalizeVector3f(vNorms[1]);
    m3dNormalizeVector3f(vNorms[2]);
    
    
    // Search for match - triangle consists of three verts
    for(GLuint iVertex = 0; iVertex < 3; iVertex++)
    {
        GLuint iMatch = 0;
        for(iMatch = 0; iMatch < glTriangle->nNumVerts; iMatch++)
        {
            // If the vertex positions are the same
            if(m3dCloseEnoughf(glTriangle->pVerts[iMatch][0], verts[iVertex][0], e) &&
               m3dCloseEnoughf(glTriangle->pVerts[iMatch][1], verts[iVertex][1], e) &&
               m3dCloseEnoughf(glTriangle->pVerts[iMatch][2], verts[iVertex][2], e) &&
               
               // AND the Normal is the same...
               m3dCloseEnoughf(glTriangle->pNorms[iMatch][0], vNorms[iVertex][0], e) &&
               m3dCloseEnoughf(glTriangle->pNorms[iMatch][1], vNorms[iVertex][1], e) &&
               m3dCloseEnoughf(glTriangle->pNorms[iMatch][2], vNorms[iVertex][2], e) &&
               
               // And Texture is the same...
               m3dCloseEnoughf(glTriangle->pTexCoords[iMatch][0], vTexCoords[iVertex][0], e) &&
               m3dCloseEnoughf(glTriangle->pTexCoords[iMatch][1], vTexCoords[iVertex][1], e))
            {
                // Then add the index only
                glTriangle->pIndexes[glTriangle->nNumIndexes] = iMatch;
                glTriangle->nNumIndexes++;
                break;
            }
        }
        
        // No match for this vertex, add to end of list
        if(iMatch == glTriangle->nNumVerts && glTriangle->nNumVerts < glTriangle->nMaxIndexes && glTriangle->nNumIndexes < glTriangle->nMaxIndexes)
        {
            memcpy(glTriangle->pVerts[glTriangle->nNumVerts], verts[iVertex], sizeof(M3DVector3f));
            memcpy(glTriangle->pNorms[glTriangle->nNumVerts], vNorms[iVertex], sizeof(M3DVector3f));
            memcpy(glTriangle->pTexCoords[glTriangle->nNumVerts], vTexCoords[iVertex], sizeof(M3DVector2f));
            glTriangle->pIndexes[glTriangle->nNumIndexes] = glTriangle->nNumVerts;
            glTriangle->nNumIndexes++;
            glTriangle->nNumVerts++;
        }
    }
}

void glTriangleBatchEnd(GLTriangle *glTriangle) {
#ifndef OPENGL_ES
	// Create the master vertex array object
	glGenVertexArrays(1, &glTriangle->vertexArrayBufferObject);
	glBindVertexArray(glTriangle->vertexArrayBufferObject);
#endif
    
    // Create the buffer objects
    glGenBuffers(4, glTriangle->bufferObjects);
    
    // Copy data to video memory
    // Vertex data
    glBindBuffer(GL_ARRAY_BUFFER, glTriangle->bufferObjects[VERTEX_DATA]);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*glTriangle->nNumVerts*3, glTriangle->pVerts, GL_STATIC_DRAW);
	glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    
    // Normal data
    glBindBuffer(GL_ARRAY_BUFFER, glTriangle->bufferObjects[NORMAL_DATA]);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*glTriangle->nNumVerts*3, glTriangle->pNorms, GL_STATIC_DRAW);
	glVertexAttribPointer(GLT_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    // Texture coordinates
    glBindBuffer(GL_ARRAY_BUFFER, glTriangle->bufferObjects[TEXTURE_DATA]);
	glEnableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*glTriangle->nNumVerts*2, glTriangle->pTexCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(GLT_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    
    // Indexes
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glTriangle->bufferObjects[INDEX_DATA]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort)*glTriangle->nNumIndexes, glTriangle->pIndexes, GL_STATIC_DRAW);
	
    
	// Done
#ifndef OPENGL_ES
	glBindVertexArray(0);
#endif
    
    // Free older, larger arrays
    if (glTriangle->pIndexes)
        free(glTriangle->pIndexes);
    if (glTriangle->pVerts)
        free(glTriangle->pVerts);
    if (glTriangle->pNorms)
        free(glTriangle->pNorms);
    if (glTriangle->pTexCoords)
        free(glTriangle->pTexCoords);
    
    // Reasign pointers so they are marked as unused
    glTriangle->pIndexes = NULL;
    glTriangle->pVerts = NULL;
    glTriangle->pNorms = NULL;
    glTriangle->pTexCoords = NULL;
    
    // Unbind to anybody
#ifndef OPENGL_ES
 	glBindVertexArray(0);
#endif
    
}

//////////////////////////////////////////////////////////////////////////
// Draw - make sure you call glEnableClientState for these arrays
void glTriangleBatchDraw(GLTriangle *glTriangle) {
#ifndef OPENGL_ES
	glBindVertexArray(glTriangle->vertexArrayBufferObject);
#else
    glBindBuffer(GL_ARRAY_BUFFER, glTriangle->bufferObjects[VERTEX_DATA]);
    glEnableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
    glVertexAttribPointer(GLT_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    // Normal data
    glBindBuffer(GL_ARRAY_BUFFER, glTriangle->bufferObjects[NORMAL_DATA]);
    glEnableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
    glVertexAttribPointer(GLT_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    // Texture coordinates
    glBindBuffer(GL_ARRAY_BUFFER, glTriangle->bufferObjects[TEXTURE_DATA]);
    glEnableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0);
    glVertexAttribPointer(GLT_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, 0);
    
    // Indexes
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glTriangle->bufferObjects[INDEX_DATA]);
#endif
    
    
    glDrawElements(GL_TRIANGLES, glTriangle->nNumIndexes, GL_UNSIGNED_SHORT, 0);
    
#ifndef OPENGL_ES
    // Unbind to anybody
	glBindVertexArray(glTriangle->vertexArrayBufferObject);
#else
    glDisableVertexAttribArray(GLT_ATTRIBUTE_VERTEX);
    glDisableVertexAttribArray(GLT_ATTRIBUTE_NORMAL);
    glDisableVertexAttribArray(GLT_ATTRIBUTE_TEXTURE0);
#endif    
}



